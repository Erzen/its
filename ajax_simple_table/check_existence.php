<?php
	// kliči DB connection datoteko
	include("../db.php");

  $table = $_POST["table"];
	$conditions = $_POST["conditions"];

	if ($table == "users") {
		$id_select = "id";
	}
	else {
		$id_select = "id_".$table;
	}

  $sWhere = "";
  $sExactMath = "";
  foreach ($conditions as $cond) {
    if ($cond["value"] == "null") {
      if ($sExactMath != "") {
        $sExactMath = $sExactMath." AND ";
      }
      $sExactMath = $sExactMath."(".$cond["key"]." = '' OR ".$cond["key"]." IS NULL)";
    }
    else {
      if ($sWhere != "") {
        $sWhere = $sWhere." AND ";
      }
      $sWhere = $sWhere.$cond["key"]." = '".$cond["value"]."'";
    }
  }

  if ($sWhere != "") {
    $sWhere = " WHERE $sWhere";
  }

  if ($sExactMath != "") {
    $sExactMath = "($sExactMath)";
  }
  else {
    $sExactMath = "1";
  }

  $sQuery = "SELECT $id_select, $sExactMath AS exact_match FROM $table$sWhere";

	$query = $db->query($sQuery);

	if($query === false) {
		$error_message = $db -> error;
		http_response_code(500);
		$output['message'] = $error_message;
		$validator = array();
	  $validator['success'] = false;
	  $validator['sql'] = $sQuery;
	  echo json_encode($validator);

	} else {
		$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
	  echo json_encode($data);
	}
	// database connection close
	$db->close();

?>
