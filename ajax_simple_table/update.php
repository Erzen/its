<?php

require_once '../db.php';

//if form is submitted
if(isset($_POST)) {

	$validator = array('success' => false, 'messages' => array());

  $table = $_POST["table"];
  $data = $_POST["data"];
  $id_key = $_POST["id_key"];
  $id_value = $_POST["id_value"];

  $pairs = Array();

  foreach($data as $key=>$value) {
    $pairs[] = "$key = '$value'";
  }

	$sql = "UPDATE $table SET ".implode(", ",$pairs)." WHERE $id_key = '$id_value'";
	$query = $db->query($sql);

	if($query === false) {
		$error_message = $db -> error;
		http_response_code(500);
		$output['message'] = $error_message;
		$validator['success'] = false;
		$validator['sql'] = $sql;
	} else {
		$validator['success'] = true;
		$validator['messages'] = "Uspešno popravljen";
	}

	// close the database connection
	$db->close();

	echo json_encode($validator);

}
