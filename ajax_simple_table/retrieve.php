<?php

require_once '../db.php';

$start = $_REQUEST["start"];
$length = $_REQUEST["length"];
$search = $_REQUEST["search"]; // search[value] je iskalni niz
$columns = $_REQUEST["columns"]; // columns[i][name] ime stolpca na i-tem index-u
$order = $_REQUEST["order"]; // order[i][column] je index referenca na stolpec po katerem sortiramo, order[i][dir] je smer po kateri urejamo (asc ali desc)

$sTable = $_REQUEST["table"];
$sIndexColumn = $_REQUEST["key_id"];

/**
* Paging
*/
$sLimit = "";
if ($length != '-1') {
  $sLimit = " LIMIT ".intval( $start ).", ".intval( $length );
}

/**
* Ordering
*/
$sOrder = "";
if ($sTable != "nalog" && count($order) > 0) {
  $column_id = $order[0]["column"];
  $order_column = $columns[$column_id]["data"];
  $order_dir = $order[0]["dir"];
  $sOrder = " ORDER BY $order_column $order_dir";
}

/**
* Filtering
* NOTE this does not match the built-in DataTables filtering which does it
* word by word on any field. It's possible to do here, but concerned about efficiency
* on very large tables, and MySQL's regex functionality is very limited
*/
$iColumnCount = count($columns);

if ( isset($search['value']) && $search['value'] != "" ) {
  $aFilteringRules = array();
  for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
    if ( isset($columns[$i]['searchable']) && $columns[$i]['searchable'] == 'true' ) {
      //$column_table = $column_tables_info[$i];
      $aFilteringRules[] = "`".$columns[$i]["data"]."` LIKE '%".$db->real_escape_string( $search['value'] )."%'";
    }
  }
  if (!empty($aFilteringRules)) {
    $aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
  }
}

if (!empty($aFilteringRules)) {
  $sWhere = " WHERE ".implode(" AND ", $aFilteringRules);
} else {
  $sWhere = "";
}

$aQueryColumns = array();
foreach ($columns as $col) {
  if ($col["data"] != ' ') {
    $aQueryColumns[] = $col["data"];
  }
}

$output = array('data' => array());


if ($sTable == "kontakt") {
  $sQuery = "(SELECT R.*,U.partner FROM `".$sTable."` R LEFT JOIN partner U on R.partner_id=U.id_partner)";
}
else if ($sTable == "oprema") {
  $sQuery = "(SELECT R.*,U.proizvajalec AS cvar_proizvajalec FROM `".$sTable."` R LEFT JOIN proizvajalec U on R.proizvajalec_id=U.id_proizvajalec)";
}
else if ($sTable == "artikel") {
  $sQuery = "(SELECT R.*,CONCAT(U.vrsta,' ',P.proizvajalec,' ',U.skupina,' ',U.model) AS oprema, P.proizvajalec AS cvar_proizvajalec, U.proizvajalec_id AS cvar_proizvajalec_id, U.vrsta AS cvar_vrsta, U.model AS cvar_model, U.skupina AS cvar_skupina FROM `".$sTable."` R LEFT JOIN oprema U on R.oprema_id=U.id_oprema LEFT JOIN proizvajalec P on U.proizvajalec_id=P.id_proizvajalec)";
}
else if ($sTable == "partner") {
  if ($_REQUEST["partner_type"] == "FO") {
    $sQuery = "(SELECT R.*, CONCAT(R.naslov,'<br>',R.posta,' ',R.mesto,'<br>',R.drzava) as naslov_concat FROM `".$sTable."` R WHERE R.fizicna_oseba = 1)";
  }
  else if ($_REQUEST["partner_type"] == "PO") {
    $sQuery = "(SELECT R.*,U.nick_name as parent_partner,CONCAT(R.naslov,'<br>',R.posta,' ',R.mesto,'<br>',R.drzava) as naslov_concat FROM `".$sTable."` R LEFT JOIN partner U on R.parent_partner_id=U.id_partner WHERE R.fizicna_oseba = 0)";
  }
  else {
    $sQuery = "(SELECT R.*,U.nick_name as parent_partner,CONCAT(R.naslov,'<br>',R.posta,' ',R.mesto,'<br>',R.drzava) as naslov_concat, IF(R.fizicna_oseba = 1, 'FO', IF(R.poslovalnica = 1, 'Poslovalnica', 'PO')) AS `partner_tip` FROM `".$sTable."` R LEFT JOIN partner U on R.parent_partner_id=U.id_partner)";
  }
}
else if ($sTable == "nalog") {

  $global_search = null;

  if (isset($_REQUEST["filters"])) {
    if (isset($_REQUEST["filters"]["global_search"])) {
      $global_search = $_REQUEST["filters"]["global_search"];
    }
    if (isset($_REQUEST["filters"]["order"])) {
      $order_column = $_REQUEST["filters"]["order"]["column"];
      $order_dir = $_REQUEST["filters"]["order"]["dir"];
      $sOrder = " ORDER BY $order_column IS NULL, $order_column $order_dir";
    }
  }

  $sQuery = "(SELECT R.*,U1.username AS cvar_odg_tehnik, P1.partner AS cvar_posiljatelj, PR1.prevoznik AS cvar_dostava, O1.*, P2.partner AS cvar_prejemnik, U2.username AS cvar_user, PR2.prevoznik AS cvar_odprema, P3.partner AS cvar_lastnik_opreme, P4.partner AS cvar_oiw_partner_id, COUNT(K.id_kosovnica) AS kosovnica_count FROM ";

  //SELECT R.id_ticket,COUNT(I.id_kosovnica) FROM nalog R LEFT JOIN kosovnica AS I ON I.nalog_id=R.id_ticket GROUP BY id_ticket;

  if ($global_search) {
    $sQuery = $sQuery."(SELECT * FROM nalog WHERE serijska = '$global_search' OR id_ticket = '$global_search') AS R ";
  }
  else {
    $sQuery = $sQuery."nalog R ";
  }

  $sQuery = $sQuery."LEFT JOIN users U1 ON R.tech_user_id=U1.id LEFT JOIN partner P1 ON R.posiljatelj_id=P1.id_partner LEFT JOIN prevoznik PR1 ON R.kurir_in_id=PR1.id_prevoznik LEFT JOIN (SELECT O.id_oprema AS cvar_id_oprema, O.vrsta AS cvar_vrsta, O.model AS cvar_model, O.skupina AS cvar_skupina, PRO1.proizvajalec AS cvar_proizvajalec, O.proizvajalec_id AS cvar_proizvajalec_id FROM oprema O LEFT JOIN proizvajalec PRO1 ON O.proizvajalec_id=PRO1.id_proizvajalec) O1 ON R.oprema_id=O1.cvar_id_oprema LEFT JOIN partner P2 ON R.prejemnik_id=P2.id_partner LEFT JOIN users U2 ON R.sales_user_id=U2.id LEFT JOIN prevoznik PR2 ON R.kurir_out_id=PR2.id_prevoznik LEFT JOIN partner P3 ON R.lastnik_id=P3.id_partner LEFT JOIN kosovnica K ON K.nalog_id=R.id_ticket LEFT JOIN partner P4 ON R.oiw_partner_id=P4.id_partner GROUP BY id_ticket)";
}
else if ($sTable == "kosovnica") {
  $nalog_id = $_REQUEST["nalog_id"];
  $sQuery = "(SELECT R.id_kosovnica, R.kolicina_kosovnica, R.porabljeno_kosovnica, A.pn AS cvar_pn, A.cena AS cvar_cena FROM (SELECT * FROM $sTable WHERE nalog_id=$nalog_id) AS R LEFT JOIN artikel AS A ON R.artikel_id=A.id_artikel)";
}
else {
  $sQuery = "`".$sTable."`";
}

$sQuery = "SELECT SQL_CALC_FOUND_ROWS main_table.* FROM ".$sQuery." AS main_table ".$sWhere.$sOrder.$sLimit;

$query = $db->query($sQuery);

if ($query == FALSE) {
  $error_message = $db -> error;
  $results = array(
    "draw"            => intval( $_REQUEST['draw'] ),
    "recordsTotal"    => 0,
    "recordsFiltered" => 0,
    "data"            => Array(),
    "error"           => $error_message,
    "sql"             => $sQuery
  );
}
else {
  // Data set length after filtering
  $sQuery = "SELECT FOUND_ROWS()";
  $totalFilteredDataResults = $db->query( $sQuery );
  list($totalFilteredData) = $totalFilteredDataResults->fetch_row();

  // Total data set length
  $sQuery = "SELECT COUNT(`".$sIndexColumn."`) FROM `".$sTable."`";
  $totalDataResults = $db->query( $sQuery );
  list($totalData) = $totalDataResults->fetch_row();

  $data = mysqli_fetch_all($query,MYSQLI_ASSOC);

  // database connection close
  $db->close();

  $results = array(
    "draw"            => intval( $_REQUEST['draw'] ),
    "recordsTotal"    => intval( $totalData ),
    "recordsFiltered" => intval( $totalFilteredData ),
    "data"            => $data
  );
}



echo json_encode($results);
