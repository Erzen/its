<?php
	// kliči DB connection datoteko
	include("../db.php");

  $autosuggest = $_GET["autosuggest"];

	$global_search = isset($autosuggest["global_search"]) ? $autosuggest["global_search"] : null;

  $table = isset($autosuggest["table"]) ? $autosuggest["table"] : null;
  $key_id = isset($autosuggest["key"]) ? $autosuggest["key"] : null;
  $term = isset($_GET["term"]) ? $_GET["term"] : null;
  $by_id = isset($autosuggest["by_id"]) ? $autosuggest["by_id"] : null;
	$by_enum = isset($autosuggest["by_enum"]) ? $autosuggest["by_enum"] : null;
	$filter = isset($autosuggest["filter"]) ? $autosuggest["filter"] : null;
	$dropdown = isset($autosuggest["dropdown"]) ? true : false; //$autosuggest["dropdown"];

	$tree_structure = isset($autosuggest["tree_structure"]) ? $autosuggest["tree_structure"] : null;
	$tree_key = isset($autosuggest["tree_key"]) ? $autosuggest["tree_key"] : null;
	$tree_special = isset($autosuggest["tree_special"]) ? $autosuggest["tree_special"] : null;
	$extra_conditions = isset($_GET["extra_conditions"]) ? $_GET["extra_conditions"]: null;

	if ($table == "users") {
		$id_select = "id";
	}
	else {
		$id_select = "id_".$table;
	}

	if ($by_enum) {
		$query = $db->query( "SELECT COLUMN_TYPE FROM information_schema.COLUMNS WHERE TABLE_NAME='$table' AND COLUMN_NAME='$key_id'");
		$enums = mysqli_fetch_all($query,MYSQLI_ASSOC)[0]["COLUMN_TYPE"];
    preg_match("/^enum\(\'(.*)\'\)$/", $enums, $matches);
    $data = explode("','", $matches[1]);
		foreach ($data as &$value) {
	    $value = array("value" => $value);
		}
		echo json_encode($data);
	}
	else {
		if ($global_search) {
			$sQuery = "SELECT * FROM nalog WHERE CAST(id_ticket as CHAR) LIKE '$term%' OR serijska LIKE '$term%'";
		}
		else {

			$sWhere = "";

			if ($tree_structure && !empty($extra_conditions)) {
				foreach ($extra_conditions as $cond) {
					if ($filter != "") {
						$filter = $filter." AND ";
					}
				  $filter = $filter.$cond["key"]." = '".$cond["value"]."'";
				}
			}

			if ($tree_structure && $tree_special) {
				$table = $autosuggest["tree_table"];
				$key_id = $autosuggest["tree_id"];
			}

			if ($dropdown) {
				$sWhere = "";
			}
			else {
				if ($tree_structure && $tree_special) {
					if ($filter != "") {
						$sWhere = " WHERE ".$filter;
					}
				}
				else {
					$sWhere = " WHERE ".$key_id." LIKE '".$term."%'";
					if ($filter != "") {
						$sWhere = $sWhere." AND ".$filter;
					}
				}

			}

			if ($by_id == true && !($tree_structure)) {
				if ($table == "partner") {
					$sQuery = "SELECT $id_select as id, CONCAT(".$key_id.",': ', naslov, ',', posta, ' ', mesto) as value FROM ".$table.$sWhere;
				}
				else {
					$sQuery = "SELECT $id_select as id, ".$key_id." as value FROM ".$table.$sWhere;
				}
			}
			else {
				$sQuery = "SELECT DISTINCT(".$key_id.") as value FROM ".$table.$sWhere;
			}

			if ($tree_structure && $tree_special) {
				if ($by_id) {
					$sQuery = "SELECT B.".$id_select." AS id, ".$autosuggest["key"]." AS value FROM (".$sQuery.") AS A LEFT JOIN ".$autosuggest["table"]." B ON A.value=B.".$id_select." WHERE B.".$autosuggest["key"]." LIKE '".$term."%'";
				}
				else {
					$sQuery = "SELECT B.".$autosuggest["key"]." AS value FROM (".$sQuery.") AS A LEFT JOIN ".$autosuggest["table"]." B ON A.value=B.".$autosuggest["key"]." WHERE B.".$id_select." LIKE '".$term."%'";
				}
			}

			/*if ($table == "oprema" && $key_id == "custom_oprema_key") {
				$sQuery = "SELECT A.* FROM (SELECT $id_select AS id,CONCAT(R.vrsta,' ',P.proizvajalec, ' ',R.skupina, ' ', R.model) as value FROM ".$table." R LEFT JOIN proizvajalec P on R.proizvajalec_id=P.id_proizvajalec) A WHERE A.value LIKE '%".$term."%'";
			}
			else {

			}*/
		}

		$query = $db->query($sQuery);

		if($query === false) {
			$error_message = $db -> error;
			http_response_code(500);
			$output['message'] = $error_message;
			$validator['success'] = false;
			$validator['sql'] = $sQuery;
			echo json_encode($validator);
		} else {
			$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
			echo json_encode($data);
		}

	}

	// database connection close
	$db->close();



?>
