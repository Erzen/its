<?php

require_once '../db.php';

$sTable = $_POST["table"];
$id_value = $_POST["id_value"];

if (isset($_POST["id_key"])) {
  $id_key = $_POST["id_key"];

}
else {
  if ($sTable == "users") {
    $id_key = "id";
  }
  else {
    $id_key = "id_".$sTable;
  }
}


if ($sTable == "kontakt") {
  $sQuery = "(SELECT R.*,U.partner FROM `".$sTable."` R LEFT JOIN partner U on R.partner_id=U.id_partner)";
}
else if ($sTable == "oprema") {
  $sQuery = "(SELECT R.*,U.proizvajalec AS cvar_proizvajalec FROM `".$sTable."` R LEFT JOIN proizvajalec U on R.proizvajalec_id=U.id_proizvajalec)";
}
else if ($sTable == "artikel") {
  $sQuery = "(SELECT R.*,CONCAT(U.vrsta,' ',P.proizvajalec) as oprema, P.proizvajalec AS cvar_proizvajalec, U.proizvajalec_id AS cvar_proizvajalec_id, U.vrsta AS cvar_vrsta, U.model AS cvar_model, U.skupina AS cvar_skupina FROM `".$sTable."` R LEFT JOIN oprema U on R.oprema_id=U.id_oprema LEFT JOIN proizvajalec P on U.proizvajalec_id=P.id_proizvajalec)";
}
else if ($sTable == "partner") {
  if (!isset($_REQUEST["partner_type"])) {
    $sQuery = "(SELECT R.*,U.nick_name as parent_partner,CONCAT(R.naslov,'<br>',R.posta,' ',R.mesto,'<br>',R.drzava) as naslov_concat,  IF(R.fizicna_oseba = 1, 'FO', IF(R.poslovalnica = 1, 'Poslovalnica', 'PO')) AS `partner_tip`  FROM `".$sTable."` R LEFT JOIN partner U on R.parent_partner_id=U.id_partner)";
  }
  else if ($_REQUEST["partner_type"] == "FO") {
    $sQuery = "(SELECT R.*, CONCAT(R.naslov,'<br>',R.posta,' ',R.mesto,'<br>',R.drzava) as naslov_concat FROM `".$sTable."` R WHERE R.fizicna_oseba = 1)";
  }
  else if ($_REQUEST["partner_type"] == "PO") {
    $sQuery = "(SELECT R.*,U.nick_name as parent_partner,CONCAT(R.naslov,'<br>',R.posta,' ',R.mesto,'<br>',R.drzava) as naslov_concat FROM `".$sTable."` R LEFT JOIN partner U on R.parent_partner_id=U.id_partner WHERE R.poslovalnica = 1)";
  }
}
else if ($sTable == "nalog") {
  $sQuery = "(SELECT R.*,U1.username AS odg_tehnik, P1.partner AS posiljatelj, PR1.prevoznik AS dostava, O1.*, P2.partner AS prejemnik, U2.username AS user, PR2.prevoznik AS odprema FROM nalog R LEFT JOIN users U1 ON R.tech_user_id=U1.id LEFT JOIN partner P1 ON R.posiljatelj_id=P1.id_partner LEFT JOIN prevoznik PR1 ON R.kurir_in_id=PR1.id_prevoznik LEFT JOIN (SELECT O.id_oprema,O.vrsta,O.model,O.skupina,PRO1.proizvajalec FROM oprema O LEFT JOIN proizvajalec PRO1 ON O.proizvajalec_id=PRO1.id_proizvajalec) O1 ON R.oprema_id=O1.id_oprema LEFT JOIN partner P2 ON R.prejemnik_id=P2.id_partner LEFT JOIN users U2 ON R.sales_user_id=U2.id LEFT JOIN prevoznik PR2 ON R.kurir_out_id=PR2.id_prevoznik)";
}
else {
  $sQuery = $sTable;
}

$sQuery = "SELECT A.* FROM $sQuery A WHERE A.$id_key = '$id_value'";
$query = $db->query($sQuery);

if($query === false) {
  $error_message = $db -> error;
  http_response_code(500);
  $output['message'] = $error_message;
  $validator = array();
  $validator['success'] = false;
  $validator['sql'] = $sQuery;
  echo json_encode($validator);

} else {
  $result = $query->fetch_assoc();
  echo json_encode($result);
}

$db->close();
