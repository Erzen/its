<?php

require_once '../db.php';

//if form is submitted
if($_POST) {

	$validator = array('success' => false, 'messages' => array());

  $table = $_POST["table"];
  $data = $_POST["data"];

  $keys = Array();
  $values = Array();

	$id_key = null;
	if ($table == "users") {
	  $id_key = "id";
	}
	else if ($table == "nalog") {
		$id_key = "id_ticket";
	}
	else {
	  $id_key = "id_".$table;
	}

  foreach($data as $key=>$value) {
    $keys[] = $key;
    $values[] = str_replace("'","\'",$value);
  }

$keys[] = "zadnji_spreminjal";
$values[] = $_SESSION["userid"];


  $sql = "INSERT INTO $table (".implode( ', ' , $keys ).") VALUES ('".implode("', '", $values)."') RETURNING $id_key AS id";
	$query = $db->query($sql);

	if($query == false) {
		$error_message = $db -> error;
		http_response_code(500);
		$validator['message'] = $error_message;
		$validator['success'] = false;
		$validator['sql'] = $sql;

	} else {
	$validator['success'] = true;
		$validator['messages'] = "Successfully Added";
		$validator['inserted'] = $query->fetch_assoc();
	}

	// close the database connection
	$db->close();

	echo json_encode($validator);

}
?>
