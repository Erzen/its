<?php
  if(!isset($_SESSION)) {
    session_start();
  }

  $key = isset($_REQUEST["key"]) ? $_REQUEST["key"] : null;
  $value = isset($_REQUEST["value"]) ? $_REQUEST["value"] : null;

  if ($value) {
    $_SESSION[$key] = $value;
  }
  else if (isset($_SESSION[$key])) {
    unset($_SESSION[$key]);
  }

  echo true;
?>
