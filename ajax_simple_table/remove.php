<?php

require_once '../db.php';

$output = array('success' => false, 'messages' => array());

$table = $_POST["table"];
$id_key = $_POST["id_key"];
$id_value = $_POST["id_value"];

$sql = "DELETE FROM ".$table." WHERE ".$id_key." = {$id_value}";
$query = $db->query($sql);

if($query === TRUE) {
	$output['success'] = true;
	$output['messages'] = 'Successfully removed';
} else {

	$error_message = $db -> error;

	if (strpos($error_message, 'foreign key constraint fails') !== false) {
		$output['message'] = "Vnos, ki ga želite izbrisati, se pojavi v vsaj enem nalogu!";
	}
	else {
		$output['message'] = $error_message;
	}

	http_response_code(500);
	$output['success'] = false;
	$output['query'] = $sql;
}

// close database connection
$db->close();

echo json_encode($output);
