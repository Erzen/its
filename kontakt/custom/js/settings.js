globals.variable_settings = {
  urls: {
    api_url: "../ajax_simple_table/retrieve.php",
    create_url: "../ajax_simple_table/create.php",
    remove_url: "../ajax_simple_table/remove.php",
    get_selected_url: "../ajax_simple_table/getSelected.php",
    update_url: "../ajax_simple_table/update.php"
  },
  texts: {
    create: "Dodaj kontakt",
    edit: "Uredi kontakt"
  },
  key: "kontakt",
  id_key: "id_kontakt",
  text: "Osnovni podatki",
  data: [
    {
      key: "partner",
      text: "Ime podjetja",
      new_ticket_info: true,
      input_type: "autosuggest",
      by_id_key: "partner_id",
      required: true,
      add_new: true,
      add_new_url: "../partner/dodajPartnerja.php?type='PO'",
      autosuggest: {
        table: "partner",
        key: "partner",
        by_id: true,
        filter: "fizicna_oseba = 0"
      },
      autosuggest_save: {
        key: "partner_id"
      }
    },
    {
      key: "imeinpriimek",
      text: "Ime in priimek",
      new_ticket_info: true,
      input_type: "string",
      required: true
    },
    {
      key: "telefon",
      text: "Telefon",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true
    },
    {
      key: "mail",
      text: "Email naslov",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true
    },
    {
      key: "zaposlitev",
      text: "Delovno mesto",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    },
    {
      key: "kontakt_opomba",
      text: "Komentar",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }/*,
    {
      key: "trn_date",
      text: "trn_date",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    },
    {
      key: "zadnji_spreminjal",
      text: "zadnji_spreminjal",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }*/
  ]
}
