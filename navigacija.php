<?php
  define('IMAGES_FOLDER', '' . '/resources/images');
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><img src="<?php echo IMAGES_FOLDER; ?>/its_logo.png" alt="ITS Intertrade"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li>
          <form class="form-inline my-2 my-lg-0" action="../resources/custom/php/global_search_redirect.php" method="post">
            <input id="global-search" class="form-control mr-sm-2" type="search" placeholder="Najdi" aria-label="Search" name="global_search">
          </form>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; Nalogi
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Moji nalogi</a>
                <a class="dropdown-item" href="#">Odprti nalogi</a>
                <a class="dropdown-item" href="#">Zapadli nalogi</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/nalog">Vsi nalogi</a>
            </div>

        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <i class="fa fa-handshake-o" aria-hidden="true"></i>&nbsp; Partnerji
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="/partner?type='all'">Vse stranke</a>
                <a class="dropdown-item" href="/partner?type='FO'">Stranke FO</a>
                <a class="dropdown-item" href="/partner?type='PO'">Stranke PO</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/prevozniki">Prevozniki</a>
                <a class="dropdown-item" href="/proizvajalci">Proizvajalci</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/kontakt"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; Kontakti</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/oprema"><i class="fa fa-tv" aria-hidden="true"></i>&nbsp; Oprema</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/artikel"><i class="fa fa-gears" aria-hidden="true"></i>&nbsp; Artikli</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp; Statistika</a>
        </li>
        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp; Nastavitve
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Link 1</a>
                <a class="dropdown-item" href="#">Link 2</a>
                <a class="dropdown-item" href="#">Link 3</a>
            </div>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <a class="nav-link" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/users/logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Odjava: <?php echo $_SESSION['username']; ?>!<span class="sr-only">(current)</span></a>
      </form>
    </div>
  </nav>
