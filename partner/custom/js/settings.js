globals.variable_settings = {
  urls: {
    api_url: "../ajax_simple_table/retrieve.php",
    create_url: "../ajax_simple_table/create.php",
    remove_url: "../ajax_simple_table/remove.php",
    get_selected_url: "../ajax_simple_table/getSelected.php",
    update_url: "../ajax_simple_table/update.php"
  },
  texts: {
    create: "Dodaj partenerja",
    edit: "Uredi partnerja",
    remove_poslovalnica: "Ali želite izbrisati partnerja z vsemi pripadajočimi poslovalnicami in kontakti?",
    remove_ostalo: "Ali želite izbrisati partnerja z vsemi pripadajočimi kontakti?"
  },
  key: "partner",
  id_key: "id_partner",
  text: "Osnovni podatki",
  data: [
    {
      key: "partner_tip",
      text: "Tip",
      input_type: "string",
      partner_type: ["all"]
    },
    {
      key: "parent_partner",
      text: "Matična poslovalnica",
      new_ticket_info: true,
      input_type: "autosuggest",
      by_id_key: "parent_partner_id",
      editable: true,
      partner_type: ["PO"],
      autosuggest: {
        table: "partner",
        key: "partner",
        filter: "poslovalnica = 0 AND fizicna_oseba = 0",
        by_id: true
      },
      autosuggest_save: {
        key: "parent_partner_id"
      },
      autosuggest_onchange: function(ui) {
        if (!ui.item) {
          $("#custom-input-id-idzaddv").attr("disabled",false);
          $("#custom-input-id-idzaddv").val("");
        }
        else {
          Ajax(simple_table_urls.get_selected_url, {table: "partner", id_value: ui.item.id}, function(rowInfo) {
            $("#custom-input-id-idzaddv").attr("disabled",true);
            $("#custom-input-id-idzaddv").val(JSON.parse(rowInfo).idzaddv);
          });
        }
      }
    },
    {
      key: "partner",
      text: "Partner",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      partner_type: ["PO","FO","all"],
      required: true
    },
    {
      key: "nick_name",
      text: "Kratko ime",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      partner_type: ["PO"],
      required: true
    },
    {
      key: "naslov",
      text: "Naslov",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true,
      partner_type: ["PO","FO","all"]
    },
    {
      key: "posta",
      text: "Pošta",
      new_ticket_info: true,
      input_type: "string",
      partner_type: ["PO","FO","all"],
      editable: true,
      required: true
    },
    {
      key: "mesto",
      text: "Mesto",
      new_ticket_info: true,
      input_type: "string",
      partner_type: ["PO","FO","all"],
      editable: true,
      required: true
    },
    {
      key: "drzava",
      text: "Država",
      new_ticket_info: true,
      input_type: "string",
      partner_type: ["PO","FO","all"],
      editable: true,
      required: true
    },
    {
      key: "idzaddv",
      text: "DDV",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      partner_type: ["PO","FO","all"]
    },
    {
      key: "telefon",
      text: "Telefon",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      partner_type: ["PO","FO","all"],
      required: true
    },
    {
      key: "email",
      text: "E-mail",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true,
      partner_type: ["PO","FO","all"]
    },
    {
      key: "partner_opomba",
      text: "Komentar",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      partner_type: ["PO","FO","all"]
    }/*,
    {
      key: "zadnji_spreminjal",
      text: "zadnji_spreminjal",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }*/
  ]
}
