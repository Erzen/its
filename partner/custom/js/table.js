let simple_table_urls = {
  api_url: "../ajax_simple_table/retrieve.php",
  create_url: "../ajax_simple_table/create.php",
  remove_url: "../ajax_simple_table/remove.php",
  get_selected_url: "../ajax_simple_table/getSelected.php",
  update_url: "../ajax_simple_table/update.php"
}

function makeTable(partnerType) {

  let columns = [];

  for (var i=0; i<globals.variable_settings.data.length; i++) {
    let data = globals.variable_settings.data[i];
    if (data.partner_type.indexOf(partnerType) != -1) {
      if (data.key == "parent_partner") {
        columns.push({data: data.key, title: data.text, defaultContent: "Matična"})
      }
      else {
        columns.push({data: data.key, title: data.text, defaultContent: "None"})
      }
    }
  }

  let buttons = [
    {
      text: '<i class="fas fa-plus-circle"></i> '+globals.variable_settings.texts.create,
      action: function ( e, dt, node, config ) {
        $("#modal").modal("show");
        dodajVnos(partnerType);
      },
      init: function(api, node, config) {
        $(node).removeClass('dt-button')
      },
      className: "btn btn-custom"
    }
  ];

  let table = document.createElement("table");
  $(table).addClass("table table-striped wrap dataTable dtr-inline collapsed");
  $(table).attr("id","table-"+globals.variable_settings.key);
  $(".table-wrapper").append(table);

  columns[columns.length-1].fnCreatedCell = function (nTd, sData, oData, iRow, iCol) {
    $(nTd).html(sData);

    let actions = document.createElement("div");
    $(actions).addClass("dropdown");
    $(actions).attr("style","margin-left:10px; float: right; display: inline-block;");
    $(nTd).append(actions);

    let i = document.createElement("i");
    $(i).addClass("fas fa-cog dropdown-toggle").attr("data-toggle","dropdown").attr("aria-haspopup","true").attr("aria-expanded","false");
    $(actions).append(i);

    let actionsListDiv = document.createElement("div");
    $(actionsListDiv).addClass("dropdown-menu dropdown-menu-right");
    $(actions).append(actionsListDiv);

    let aUredi = document.createElement("a");
    $(aUredi).addClass("dropdown-item").attr("href","#");
    $(aUredi).append("Uredi");
    $(aUredi).click(function() {
      let params = {};
      let id_key = globals.variable_settings.id_key;
      params.id_key = id_key;
      params.id_value = oData[id_key];
      params.table = globals.variable_settings.key;
      urediVnos(params);
    });
    $(actionsListDiv).append(aUredi);

    if (oData.fizicna_oseba == 0) {
      let aKontakti = document.createElement("a");
      $(aKontakti).addClass("dropdown-item").attr("href","#");
      $(aKontakti).append("Kontakti");
      $(aKontakti).click(function() {
        let params = {};
        let id_key = globals.variable_settings.id_key;
        params.id_key = "partner_id";
        params.id_value = oData[id_key];
        params.table = "kontakt";
        pokaziKontakte(oData.partner,params);
      });
      $(actionsListDiv).append(aKontakti);
    }

    let aIzbrisi = document.createElement("a");
    $(aIzbrisi).addClass("dropdown-item").attr("href","#");
    $(aIzbrisi).append("Izbriši");
    $(aIzbrisi).click((function(table) {
      return function() {
        let params = {};
        let id_key = globals.variable_settings.id_key;
        params.id_key = id_key;
        params.id_value = oData[id_key];
        params.table = globals.variable_settings.key;
        if (oData.poslovalnica == 0 && oData.fizicna_oseba == 0) {
          if (confirm(globals.variable_settings.texts.remove_poslovalnica) == true) {
            Ajax(simple_table_urls.remove_url,params, function() {
              $(table).DataTable().ajax.reload(null,false);
            });
          }
          else {
            return;
          }
        }
        else {
          if (confirm(globals.variable_settings.texts.remove_ostalo) == true) {
            Ajax(simple_table_urls.remove_url,params, function() {
              $(table).DataTable().ajax.reload(null,false);
            });
          }
          else {
            return;
          }
        }
      }
    })(table));
    $(actionsListDiv).append(aIzbrisi);

  }

  var tableContent = $(table).DataTable({
    serverSide: true,
    ajax: {
      url: simple_table_urls.api_url,
      type: 'POST',
      dataType: 'JSON',
      data: {
        table: globals.variable_settings.key,
        key_id: globals.variable_settings.id_key,
        partner_type: partnerType
      }
    },
    language: {
      emptyTable: "Ni prevoznikov."
    },
    dom: "<'row mb-2'<'col-md-12'B>><'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
    columns: columns,
    buttons: buttons
  })

  return tableContent;
}

function dodajVnos(partnerType) {

  openModal("modal",{clear: true, title: '<i class="fas fa-plus-circle"></i> '+globals.variable_settings.texts.create});

  let wrapperDiv = document.createElement("div");
  $(wrapperDiv).css("width","100%");
  $("#modal .modal-body").append(wrapperDiv);

  if (partnerType == "all") {
    let selectTypeDiv = document.createElement("div");
    $(selectTypeDiv).append('<span class="bold-text">Tip: </span>');
    $(selectTypeDiv).css("padding-bottom","10px").css("margin-bottom","10px").css("border-bottom","1px solid gray");
    $(wrapperDiv).append(selectTypeDiv);

    let selectTypeSpan = document.createElement("span");
    $(selectTypeSpan).addClass("details-content");
    $(selectTypeDiv).append(selectTypeSpan);

    let formCheckPO = document.createElement("div");
    $(formCheckPO).addClass("form-check form-check-inline");
    $(formCheckPO).append('<input class="form-check-input" type="radio" name="tipPartnerjaRadio" value="PO" checked>');
    $(formCheckPO).append('<label class="form-check-label" for="inlineCheckbox1">Pravna oseba</label>');
    $(selectTypeSpan).append(formCheckPO);

    let formCheckFO = document.createElement("div");
    $(formCheckFO).addClass("form-check form-check-inline");
    $(formCheckFO).append('<input class="form-check-input" type="radio" name="tipPartnerjaRadio" value="FO">');
    $(formCheckFO).append('<label class="form-check-label" for="inlineCheckbox1">Fizična oseba</label>');
    $(selectTypeSpan).append(formCheckFO);

    $('input[type=radio][name=tipPartnerjaRadio]').change(function() {
      let dataToFill = dataPoTipu(this.value);
      $(div).html("");
      fillDetails(div,null,dataToFill,"new");
    });
  }

  let div = document.createElement("div");

  let dataToFill = dataPoTipu(partnerType);

  fillDetails(div,null,dataToFill,"new");

  $(wrapperDiv).append(div);

  let buttonSave = document.createElement("button");
  $(buttonSave).addClass("btn btn-custom save-button");
  $(buttonSave).append("Shrani");
  $(buttonSave).click(function() {
    let div = $("#modal .modal-body");
    let prevoznik = {}

    readSection(div,{rows: [{columns: [{data: globals.variable_settings.data}]}]}, function(key,value) {
      prevoznik[key] = value
    })

    if ($(div).find(".missing-value").length == 0) {
      console.log(prevoznik.nick_name == undefined)
      if (prevoznik.nick_name == undefined) {
        prevoznik.nick_name = prevoznik.partner
      }
      if (partnerType == "all") {
        let currPartnerType = $("input[name='tipPartnerjaRadio']:checked").val();
        prevoznik.poslovalnica = currPartnerType == "PO" ? 1 : 0;
        prevoznik.fizicna_oseba = currPartnerType == "FO" ? 1 : 0;
      }
      else {
        prevoznik.poslovalnica = partnerType == "PO" ? 1 : 0;
        prevoznik.fizicna_oseba = partnerType == "FO" ? 1 : 0;
      }

      if (prevoznik.poslovalnica) {
        // ni maticne poslovalnice => preveri ali zeli shraniti kot maticno poslovalnico
        if (prevoznik.parent_partner_id == undefined) {
          if (confirm('Matična poslovalnica ni vpisana. Ali želite shraniti stranko kot matično poslovalnico?')) {
            prevoznik.poslovalnica = 0;
            Ajax(simple_table_urls.create_url,{table: globals.variable_settings.key, data: prevoznik}, function(results) {
              $("#table-"+globals.variable_settings.key).DataTable().ajax.reload(null,true);
              if (typeof sendVnosToParent == "function") {
                sendVnosToParent(results)
              }
            });
            $("#modal").modal("hide");
          } else {
            // Do nothing!
          }
        }
        else {
          Ajax(simple_table_urls.create_url,{table: globals.variable_settings.key, data: prevoznik}, function(results) {
            $("#table-"+globals.variable_settings.key).DataTable().ajax.reload(null,true);
            if (typeof sendVnosToParent == "function") {
              sendVnosToParent(results)
            }
          });
          $("#modal").modal("hide");
        }
      }
      else {
        Ajax(simple_table_urls.create_url,{table: globals.variable_settings.key, data: prevoznik}, function(results) {
          $("#table-"+globals.variable_settings.key).DataTable().ajax.reload(null,true);
          if (typeof sendVnosToParent == "function") {
            sendVnosToParent(results)
          }
        });
        $("#modal").modal("hide");
      }
    }
  })

  $("#modal .modal-footer").html("").append(buttonSave);
}

function urediVnos(params) {

  Ajax(simple_table_urls.get_selected_url, params, function(rowInfo) {

    openModal("modal",{clear: true, title: '<i class="fas fa-plus-circle"></i> '+globals.variable_settings.texts.edit});

    let dataType = rowInfo.fizicna_oseba ? "FO" : "PO";

    let dataToFill = dataPoTipu(dataType);

    let wrapperDiv = document.createElement("div");
    $(wrapperDiv).css("width","100%");
    $("#modal .modal-body").append(wrapperDiv)

    let div = document.createElement("div");
    fillDetails(div,JSON.parse(rowInfo),dataToFill,"edit");

    $(wrapperDiv).append(div);

    let buttonSave = document.createElement("button");
    $(buttonSave).addClass("btn btn-custom");
    $(buttonSave).append("Shrani");
    $(buttonSave).click(function() {
      let div = $("#modal .modal-body");
      let prevoznik = {}

      readSection(div,{rows: [{columns: [{data: globals.variable_settings.data}]}]}, function(key,value) {
        prevoznik[key] = value
      })

      if ($(div).find(".missing-value").length == 0) {
        console.log(params)
        Ajax(simple_table_urls.update_url,{table: params.table, data: prevoznik, id_key: params.id_key, id_value: params.id_value}, function(results) {
          $("#table-"+globals.variable_settings.key).DataTable().ajax.reload(null,true);
        });
        $("#modal").modal("hide");
      }
    })

    $("#modal .modal-footer").html("").append(buttonSave);
  })

}

function pokaziKontakte(partner,params) {
  Ajax(simple_table_urls.get_selected_url, params, function(rowInfo) {
    rowInfo = JSON.parse(rowInfo);

    if (rowInfo == null) {
      rowInfo = [];
    }
    else if (!(Array.isArray(rowInfo))) {
      rowInfo = [rowInfo];
    }

    openModal("modal",{size: "lg", clear: true, title: "Kontakti "+partner});

    let wrapperDiv = document.createElement("div");
    $(wrapperDiv).css("width","100%");
    $("#modal .modal-body").append(wrapperDiv)

    let table = document.createElement("table");
    $(table).addClass("table table-striped wrap dataTable dtr-inline collapsed");
    $(wrapperDiv).append(table);

    $(table).DataTable({
      data: rowInfo,
      language: {
        emptyTable: "Ni kontaktov."
      },
      columns: [
        {
          data: "imeinpriimek",
          title: "Ime in priimek"
        },
        {
          data: "telefon",
          title: "Telefon"
        },
        {
          data: "mail",
          title: "Email naslov"
        },
        {
          data: "zaposlitev",
          title: "Zaposlitev"
        },
        {
          data: "kontakt_opomba",
          title: "Komentar"
        }
      ]
    })

  });
}

function dataPoTipu(partnerType) {

  partnerType = (partnerType == "all") ? "PO" : partnerType;

  let dataToFill = {rows: [{columns: [{data: []}]}]}

  for (var i=0; i<globals.variable_settings.data.length; i++) {
    let data = globals.variable_settings.data[i];
    if (data.partner_type.indexOf(partnerType) != -1) {
      dataToFill.rows[0].columns[0].data.push(data);
    }
  }

  return dataToFill;
}
