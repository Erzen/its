<?php
  define('VENDOR_FOLDER', '' . '/resources/vendor');
  define('CUSTOM_FOLDER', '' . '/resources/custom');
?>


<head>

  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">

  <!-- Vsi importi za celotno stran -->
  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/select.bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/responsive.bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/@fortawesome/fontawesome-free/css/all.css">

  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo VENDOR_FOLDER; ?>/css/buttons.dataTables.min.css">

  <link rel="stylesheet" href="<?php echo CUSTOM_FOLDER; ?>/css/mojslog.css">
  <link rel="stylesheet" href="<?php echo CUSTOM_FOLDER; ?>/css/custom.css">

  <script src="<?php echo VENDOR_FOLDER; ?>/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/jquery-ui.js"></script>

  <script src="<?php echo VENDOR_FOLDER; ?>/js/popper.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/dataTables.select.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo VENDOR_FOLDER; ?>/js/dataTables.buttons.min.js"></script>

  <script src="<?php echo CUSTOM_FOLDER; ?>/js/global.js"></script>
  <script src="<?php echo CUSTOM_FOLDER; ?>/js/main.js"></script>

</head>















</head>
