<?php 

require_once '../../db.php';
//include '../../function.php';

						
$output = array('data' => array());

$sql = "SELECT * FROM oprema LEFT JOIN proizvajalec ON oprema.proizvajalec_id = proizvajalec.id_proizvajalec";
$query = $db->query($sql);

$x = 1;
while ($row = $query->fetch_assoc()) {

	$actionButton = '
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" data-target="#editOpremaModal" onclick="editOprema('.$row['id_oprema'].')"> <span class="glyphicon glyphicon-edit"></span> Edit</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeOpremaModal" onclick="removeOprema('.$row['id_oprema'].')"> <span class="glyphicon glyphicon-trash"></span> Remove</a></li>	    
	  </ul>
	</div>
		';

	$output['data'][] = array(
		$x,
		$row['proizvajalec'],
		$row['vrsta'],
		$row['skupina'],
        $row['model'],
        $row['oprema_opomba'],
		$actionButton
	);

	$x++;
}

// database connection close
$db->close();

echo json_encode($output);
