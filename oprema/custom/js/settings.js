globals.variable_settings = {
  urls: {
    api_url: "../ajax_simple_table/retrieve.php",
    create_url: "../ajax_simple_table/create.php",
    remove_url: "../ajax_simple_table/remove.php",
    get_selected_url: "../ajax_simple_table/getSelected.php",
    update_url: "../ajax_simple_table/update.php"
  },
  texts: {
    create: "Dodaj opremo",
    edit: "Uredi opremo"
  },
  key: "oprema",
  id_key: "id_oprema",
  text: "Osnovni podatki",
  data: [
    {
      key: "cvar_proizvajalec",
      text: "Proizvajalec",
      new_ticket_info: true,
      required: true,
      input_type: "autosuggest",
      add_new: true,
      add_new_url: "../proizvajalci/dodajProizvajalca.php",
      by_id_key: "proizvajalec_id",
      autosuggest: {
        table: "proizvajalec",
        key: "proizvajalec",
        by_id: true
      },
      autosuggest_save: {
        key: "proizvajalec_id"
      }
    },
    {
      key: "vrsta",
      text: "vrsta",
      new_ticket_info: true,
      input_type: "autosuggest",
      required: true,
      autosuggest: {
        table: "oprema",
        key: "vrsta",
        must_exist_in_db: false
      },
      autosuggest_save: {
        key: "vrsta"
      }
    },
    {
      key: "skupina",
      text: "skupina",
      new_ticket_info: true,
      input_type: "string",
      required: true
    },
    {
      key: "model",
      text: "model",
      new_ticket_info: true,
      input_type: "string"
    },
    {
      key: "oprema_opomba",
      text: "Komentar",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }/*,
    {
      key: "zadnji_spreminjal",
      text: "zadnji_spreminjal",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }*/
  ]
}
