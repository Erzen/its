$(function () {
    $("#proizvajalec").change(function () {
        var displayproizvajalec = $("#proizvajalec option:selected").text();
        $("#text").val(displayproizvajalec);
    })
})

// global the manage oprema table 
var manageOpremaTable;

$(document).ready(function () {
    manageOpremaTable = $("#manageOpremaTable").DataTable({
        "ajax": "ajax_oprema/retrieveOprema.php",
        "order": []
    });

    $("#addOpremaModalBtn").on('click', function () {
        // reset the form 
        $("#createOpremaForm")[0].reset();
        // remove the error 
        $(".form-group").removeClass('has-error').removeClass('has-success');
        $(".text-danger").remove();
        // empty the message div
        $(".messages").html("");

        // submit form
        $("#createOpremaForm").unbind('submit').bind('submit', function () {

            $(".text-danger").remove();

            var form = $(this);

            // validation
            var proizvajalec = $("#proizvajalec").val();
            var vrsta = $("#vrsta").val();
            var skupina = $("#skupina").val();
            var model = $("#model").val();
            var oprema_opomba = $("#oprema_opomba").val();

            if (proizvajalec == "") {
                $("#proizvajalec").closest('.form-group').addClass('has-error');
                $("#proizvajalec").after('<p class="text-danger">Ime proizvajalca je obvezen podatek</p>');
            } else {
                $("#proizvajalec").closest('.form-group').removeClass('has-error');
                $("#proizvajalec").closest('.form-group').addClass('has-success');
            }

            if (vrsta == "") {
                $("#vrsta").closest('.form-group').addClass('has-error');
                $("#vrsta").after('<p class="text-danger">Vrsta opreme je obvezen podatek</p>');
            } else {
                $("#vrsta").closest('.form-group').removeClass('has-error');
                $("#vrsta").closest('.form-group').addClass('has-success');
            }

            if (skupina == "") {
                $("#skupina").closest('.form-group').addClass('has-error');
                $("#skupina").after('<p class="text-danger">Skupina opreme je obvezen podatek</p>');
            } else {
                $("#skupina").closest('.form-group').removeClass('has-error');
                $("#skupina").closest('.form-group').addClass('has-success');
            }

            $("#oprema_opomba").closest('.form-group').removeClass('has-error');
            $("#oprema_opomba").closest('.form-group').addClass('has-success');

            if (proizvajalec && vrsta && skupina) {
                //submit the form to server
                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: form.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        // remove the error 
                        $(".form-group").removeClass('has-error').removeClass('has-success');

                        if (response.success == true) {
                            $(".messages").html('<div class="alert alert-success alert-dismissible" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>' + response.messages +
                                '</div>');

                            // reset the form
                            $("#createOpremaForm")[0].reset();

                            // reload the datatables
                            manageOpremaTable.ajax.reload(null, false);
                            // this function is built in function of datatables;

                        } else {
                            $(".messages").html('<div class="alert alert-warning alert-dismissible" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>' + response.messages +
                                '</div>');
                        } // /else
                    } // success  
                }); // ajax submit 				
            } /// if
            return false;
        }); // /submit form for create oprema
    }); // /add modal

});

function removeOprema(id_oprema = null) {
    if (id_oprema) {
        // click on remove button
        $("#removeBtn").unbind('click').bind('click', function () {
            $.ajax({
                url: 'ajax_oprema/removeOprema.php',
                type: 'post',
                data: {
                    oprema_id: id_oprema
                },
                dataType: 'json',
                success: function (response) {
                    if (response.success == true) {
                        $(".removeMessages").html('<div class="alert alert-success alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>' + response.messages +
                            '</div>');

                        // refresh the table
                        manageOpremaTable.ajax.reload(null, false);

                        // close the modal
                        $("#removeOpremaModal").modal('hide');
                    } else {
                        $(".removeMessages").html('<div class="alert alert-warning alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>' + response.messages +
                            '</div>');
                    }
                }
            });
        }); // click remove btn
    } else {
        alert('Error: Refresh the page again');
    }
}

function editOprema(id_oprema = null) {
    if (id_oprema) {

        // remove the error 
        $(".form-group").removeClass('has-error').removeClass('has-success');
        $(".text-danger").remove();
        // empty the message div
        $(".edit-messages").html("");

        // remove the id
        $("#oprema_id").remove();

        // fetch the member data
        $.ajax({
            url: 'ajax_oprema/getSelectedOprema.php',
            type: 'post',
            data: {
                oprema_id: id_oprema
            },
            dataType: 'json',
            success: function (response) {
                $("#editProizvajalec").val(response.proizvajalec);
                $("#editVrsta").val(response.vrsta);
                $("#editSkupina").val(response.skupina);
                $("#editModel").val(response.model);
                $("#editOprema_opomba").val(response.oprema_opomba);

                // oprema id 
                $(".editOpremaModal").append('<input type="hidden" name="oprema_id" id="oprema_id" value="' + response.id_oprema + '"/>');

                // here update the member data
                $("#updateOpremaForm").unbind('submit').bind('submit', function () {
                    // remove error messages
                    $(".text-danger").remove();

                    var form = $(this);

                    // validation
                    var editProizvajalec = $("#editProizvajalec").val();
                    var editVrsta = $("#editVrsta").val();
                    var editSkupina = $("#editSkupina").val();
                    var editModel = $("#editModel").val();
                    var editOprema_opomba = $("#editOprema_opomba").val();

                    if (editProizvajalec == "") {
                        $("#editProizvajalec").closest('.form-group').addClass('has-error');
                        $("#editProizvajalec").after('<p class="text-danger">Proizvajalec je obvezen podatek</p>');
                    } else {
                        $("#editProizvajalec").closest('.form-group').removeClass('has-error');
                        $("#editProizvajalec").closest('.form-group').addClass('has-success');
                    }

                    if (editVrsta == "") {
                        $("#editVrsta").closest('.form-group').addClass('has-error');
                        $("#editVrsta").after('<p class="text-danger">Vrsta opreme je obvezen podatek</p>');
                    } else {
                        $("#editVrsta").closest('.form-group').removeClass('has-error');
                        $("#editVrsta").closest('.form-group').addClass('has-success');
                    }

                    if (editSkupina == "") {
                        $("#editSkupina").closest('.form-group').addClass('has-error');
                        $("#editSkupina").after('<p class="text-danger">Skupina opreme je obvezen podatek</p>');
                    } else {
                        $("#editSkupina").closest('.form-group').removeClass('has-error');
                        $("#editSkupina").closest('.form-group').addClass('has-success');
                    }
                    $("#editOprema_opomba").closest('.form-group').removeClass('has-error');
                    $("#editOprema_opomba").closest('.form-group').addClass('has-success');


                    if (editProizvajalec && editVrsta && editSkupina) {
                        $.ajax({
                            url: form.attr('action'),
                            type: form.attr('method'),
                            data: form.serialize(),
                            dataType: 'json',
                            success: function (response) {

                                if (response.success == true) {
                                    $(".edit-messages").html('<div class="alert alert-success alert-dismissible" role="alert">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                        '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>' + response.messages +
                                        '</div>');

                                    // reload the datatables
                                    manageOpremaTable.ajax.reload(null, false);
                                    // this function is built in function of datatables;

                                    // remove the error 
                                    $(".form-group").removeClass('has-success').removeClass('has-error');
                                    $(".text-danger").remove();
                                } else {
                                    $(".edit-messages").html('<div class="alert alert-warning alert-dismissible" role="alert">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                        '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>' + response.messages +
                                        '</div>')
                                }

                            } // /success
                        }); // /ajax
                    } // /if

                    return false;
                });

            } // /success
        }); // /fetch selected member info

    } else {
        alert("Error : Refresh the page again");
    }
}
