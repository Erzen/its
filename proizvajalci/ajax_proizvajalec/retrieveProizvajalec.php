<?php 

require_once '../../db.php';

$output = array('data' => array());

$sql = "SELECT * FROM proizvajalec";
$query = $db->query($sql);

$x = 1;
while ($row = $query->fetch_assoc()) {
//	$active = '';
//	if($row['proizvajalec_opomba'] !== '') {
//		$active = '<label class="label label-success">Active</label>';
//	} else {
//		$active = '<label class="label label-danger">Deactive</label>'; 
//	}

	$actionButton = '
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action 
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" data-target="#editProizvajalecModal" onclick="editProizvajalec('.$row['id_proizvajalec'].')"> <span class="glyphicon glyphicon-edit"></span> Edit</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeProizvajalecModal" onclick="removeProizvajalec('.$row['id_proizvajalec'].')"> <span class="glyphicon glyphicon-trash"></span> Remove</a></li>	    
	  </ul>
	</div>
		';

	$output['data'][] = array(
		$x,
		$row['proizvajalec'],
		$row['url'],
		$row['techSupport'],
        $row['proizvajalec_opomba'],
		$actionButton
	);

	$x++;
}

// database connection close
$db->close();

echo json_encode($output);
