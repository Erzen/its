<?php 

require_once '../../db.php';

$output = array('success' => false, 'messages' => array());

$proizvajalecId = $_POST['proizvajalec_id'];

$sql = "DELETE FROM proizvajalec WHERE id_proizvajalec = {$proizvajalecId}";
$query = $db->query($sql);
if($query === TRUE) {
	$output['success'] = true;
	$output['messages'] = 'Uspešno izbrisan';
} else {
	$output['success'] = false;
	$output['messages'] = 'Napaka pri brisanju podatkov';
}

// close database connection
$db->close();

echo json_encode($output);
