globals.variable_settings = {
  texts: {
    create: "Dodaj proizvajalca",
    edit: "Uredi proizvajalca",
    confirm_remove: "Ali želite izbrisati proizvajalca in vso pripadajočo opremo?"
  },
  key: "proizvajalec",
  id_key: "id_proizvajalec",
  text: "Osnovni podatki",
  data: [
    {
      key: "proizvajalec",
      text: "Ime proizvajalca",
      new_ticket_info: true,
      input_type: "string",
      required: true
    },
    {
      key: "url",
      text: "url",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true
    },
    {
      key: "techSupport",
      text: "techSupport",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true
    },
    {
      key: "proizvajalec_opomba",
      text: "Komentar",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }
  ]
}
