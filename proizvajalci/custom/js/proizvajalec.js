// global the manage proizvajalec table 
var manageProizvajalecTable;

$(document).ready(function () {
    manageProizvajalecTable = $("#manageProizvajalecTable").DataTable({
        "ajax": "ajax_proizvajalec/retrieveProizvajalec.php",
        "order": []
    });

    $("#addProizvajalecModalBtn").on('click', function () {
        // reset the form 
        $("#createProizvajalecForm")[0].reset();
        // remove the error 
        $(".form-group").removeClass('has-error').removeClass('has-success');
        $(".text-danger").remove();
        // empty the message div
        $(".messages").html("");

        // submit form
        $("#createProizvajalecForm").unbind('submit').bind('submit', function () {

            $(".text-danger").remove();

            var form = $(this);

            // validation
            var proizvajalec = $("#proizvajalec").val();
            var url = $("#url").val();
            var techSupport = $("#techSupport").val();
            var proizvajalec_opomba = $("#proizvajalec_opomba").val();

            if (proizvajalec == "") {
                $("#proizvajalec").closest('.form-group').addClass('has-error');
                $("#proizvajalec").after('<p class="text-danger">Ime proizvajalca je obvezen podatek</p>');
            } else {
                $("#proizvajalec").closest('.form-group').removeClass('has-error');
                $("#proizvajalec").closest('.form-group').addClass('has-success');
            }

            if (url == "") {
                $("#url").closest('.form-group').addClass('has-error');
                $("#url").after('<p class="text-danger">URL je obvezen podatek</p>');
            } else {
                $("#url").closest('.form-group').removeClass('has-error');
                $("#url").closest('.form-group').addClass('has-success');
            }

            if (techSupport == "") {
                $("#techSupport").closest('.form-group').addClass('has-error');
                $("#techSupport").after('<p class="text-danger">Spletni naslov tehnične podpore je obvezen podatek</p>');
            } else {
                $("#techSupport").closest('.form-group').removeClass('has-error');
                $("#techSupport").closest('.form-group').addClass('has-success');
            }

            $("#proizvajalec_opomba").closest('.form-group').removeClass('has-error');
            $("#proizvajalec_opomba").closest('.form-group').addClass('has-success');

            if (proizvajalec && url && techSupport) {
                //submit the form to server
                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: form.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        // remove the error 
                        $(".form-group").removeClass('has-error').removeClass('has-success');

                        if (response.success == true) {
                            $(".messages").html('<div class="alert alert-success alert-dismissible" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>' + response.messages +
                                '</div>');

                            // reset the form
                            $("#createProizvajalecForm")[0].reset();

                            // reload the datatables
                            manageProizvajalecTable.ajax.reload(null, false);
                            // this function is built in function of datatables;

                        } else {
                            $(".messages").html('<div class="alert alert-warning alert-dismissible" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>' + response.messages +
                                '</div>');
                        } // /else
                    } // success  
                }); // ajax submit 				
            } /// if
            return false;
        }); // /submit form for create proizvajalec
    }); // /add modal

});

function removeProizvajalec(id_proizvajalec = null) {
    if (id_proizvajalec) {
        // click on remove button
        $("#removeBtn").unbind('click').bind('click', function () {
            $.ajax({
                url: 'ajax_proizvajalec/removeProizvajalec.php',
                type: 'post',
                data: {
                    proizvajalec_id: id_proizvajalec
                },
                dataType: 'json',
                success: function (response) {
                    if (response.success == true) {
                        $(".removeMessages").html('<div class="alert alert-success alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>' + response.messages +
                            '</div>');

                        // refresh the table
                        manageProizvajalecTable.ajax.reload(null, false);

                        // close the modal
                        $("#removeProizvajalecModal").modal('hide');
                    } else {
                        $(".removeMessages").html('<div class="alert alert-warning alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>' + response.messages +
                            '</div>');
                    }
                }
            });
        }); // click remove btn
    } else {
        alert('Error: Refresh the page again');
    }
}

function editProizvajalec(id_proizvajalec = null) {
    if (id_proizvajalec) {

        // remove the error 
        $(".form-group").removeClass('has-error').removeClass('has-success');
        $(".text-danger").remove();
        // empty the message div
        $(".edit-messages").html("");

        // remove the id
        $("#proizvajalec_id").remove();

        // fetch the member data
        $.ajax({
            url: 'ajax_proizvajalec/getSelectedProizvajalec.php',
            type: 'post',
            data: {
                proizvajalec_id: id_proizvajalec
            },
            dataType: 'json',
            success: function (response) {
                $("#editProizvajalec").val(response.proizvajalec);
                $("#editURL").val(response.url);
                $("#editTechSupport").val(response.techSupport);
                $("#editProizvajalec_opomba").val(response.proizvajalec_opomba);

                // proizvajalec id 
                $(".editProizvajalecModal").append('<input type="hidden" name="proizvajalec_id" id="proizvajalec_id" value="' + response.id_proizvajalec + '"/>');

                // here update the member data
                $("#updateProizvajalecForm").unbind('submit').bind('submit', function () {
                    // remove error messages
                    $(".text-danger").remove();

                    var form = $(this);

                    // validation
                    var editProizvajalec = $("#editProizvajalec").val();
                    var editURL = $("#editURL").val();
                    var editTechSupport = $("#editTechSupport").val();
                    var editProizvajalec_opomba = $("#editProizvajalec_opomba").val();

                    if (editProizvajalec == "") {
                        $("#editProizvajalec").closest('.form-group').addClass('has-error');
                        $("#editProizvajalec").after('<p class="text-danger">Ime proizvajaleca je obvezen podatek</p>');
                    } else {
                        $("#editProizvajalec").closest('.form-group').removeClass('has-error');
                        $("#editProizvajalec").closest('.form-group').addClass('has-success');
                    }

                    if (editURL == "") {
                        $("#editURL").closest('.form-group').addClass('has-error');
                        $("#editURL").after('<p class="text-danger">URL naslov je obvezen podatek</p>');
                    } else {
                        $("#editURL").closest('.form-group').removeClass('has-error');
                        $("#editURL").closest('.form-group').addClass('has-success');
                    }

                    if (editTechSupport == "") {
                        $("#editTechSupport").closest('.form-group').addClass('has-error');
                        $("#editTechSupport").after('<p class="text-danger">Spletni naslov tehnične podpore je obvezen podatek</p>');
                    } else {
                        $("#editTechSupport").closest('.form-group').removeClass('has-error');
                        $("#editTechSupport").closest('.form-group').addClass('has-success');
                    }
                    $("#editProizvajalec_opomba").closest('.form-group').removeClass('has-error');
                    $("#editProizvajalec_opomba").closest('.form-group').addClass('has-success');


                    if (editProizvajalec && editURL && editTechSupport) {
                        $.ajax({
                            url: form.attr('action'),
                            type: form.attr('method'),
                            data: form.serialize(),
                            dataType: 'json',
                            success: function (response) {
                                console.log(response.success);
                                if (response.success == true) {
                                    $(".edit-messages").html('<div class="alert alert-success alert-dismissible" role="alert">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                        '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>' + response.messages +
                                        '</div>');

                                    // reload the datatables
                                    manageProizvajalecTable.ajax.reload(null, false);
                                    // this function is built in function of datatables;

                                    // remove the error 
                                    $(".form-group").removeClass('has-success').removeClass('has-error');
                                    $(".text-danger").remove();
                                } else {
                                    $(".edit-messages").html('<div class="alert alert-warning alert-dismissible" role="alert">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                        '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>' + response.messages +
                                        '</div>')
                                }

                            } // /success
                        }); // /ajax
                    } // /if

                    return false;
                });

            } // /success
        }); // /fetch selected member info

    } else {
        alert("Napaka : Potrebno je osvežiti spletno stran");
    }
}
