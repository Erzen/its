<?
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  };
?>

<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?><!DOCTYPE html>

<?php
include ('../users/auth.php');
?>

<html>

<?php
include ('../header.php');
?>

<body onLoad="odpriDodajVnos();">

  <div id="modal" class="modal" tabindex="-1" role="dialog" style="background-color: white;">
    <div class="modal-dialog modal-lg modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <!-- include custom index.js -->
  <script type="text/javascript" src="../resources/custom/js/novVnos.js"></script>
  <script type="text/javascript" src="../resources/custom/js/simple_table.js"></script>
  <script type="text/javascript" src="custom/js/settings.js"></script>

</body>

</html>
