/*

global_settings ima tri ključe:
-nalog_id_key: pove kateri key v poslanih podatkih je identiteta za posamezni nalog
-glavne_info: informacije, ki se pojavijo v vrstici tabele
-podrobnosti: so razdeljene na več ključev po vrsti informacij (npr. logistika_izhod), vsak ključ vsebuje podrobnosti, ki se pojavijo ob kliku na vrstico v tabeli

vsak predalček (glavne_info,logistika_vhod,...) ima naslednje ključe:
-text: napis, ki se pojavi ob predalčku (npr. Osnovni podatki)
-new_ticket_info: vsaj ednega od podatkov v predalčku lahko vpišemo pri odprtju novega vrsta_naloga
-rows: vsi podatki v predalčku so zaradi lažjega oblikovanja razdeljeni v vrstice in znotraj vrstic v stolpce. Podatki gredo v novo vrstico, če se v prejšnja in
nova vrstica razlikujeta v št. stolpcev (npr. v prvi vrstici so podatki razdeljeni v 3 stolpce, v drugi vrstici pa v 2)

vsak stolpec ima ključ data, ki vsebuje seznam posameznih podatkov znotraj stolpca. Podatki so podani kot slovar z naslednjimi ključi:
-key(String): isti kot key v tabeli iz backenda
-text(String): napis, ki se pojavi ob podatku (npr. Številka servisnega naloga)
-new_ticket_info(Boolean): podatek lahko vpišemo pri odprtju novega naloga
-input_type(String): tip podatka (vsi tipi so navedeni spodaj)
-editable(Boolean): podatek lahko popraviš
-required(Boolean): podatek je obvezen

input_type options:
-dropdown
-autosuggest
-date
-text
-file

*/

globals.variable_settings = {
  api_url: "../../ajax_simple_table/retrieve.php",
  create_url: "../../ajax_simple_table/create.php",
  remove_url: "../../ajax_simple_table/remove.php",
  update_url: "../../ajax_simple_table/update.php",
  get_selected_url: "../../ajax_simple_table/getSelected.php",
  session_params_url: "../../ajax_simple_table/session_params.php",
  key: "nalog",
  nalog_id_key: "id_ticket",
  id_key: "id_ticket",
  glavne_info: {
    text: "Osnovni podatki",
    rows: [
      {
        columns: [
          {
            data: [
              {
                key: "id_ticket",
                text: "Številka servisnega naloga",
                input_type: "string",
                selfgenerated: true, // auto on server side
                editable: false
              },
              {
                key: "cvar_odg_tehnik",
                text: "Odgovoren tehnik",
                input_type: "dropdown",
                multiple_occurence: true,
                first_occurance: true,
                autosuggest: {
                  table: "users",
                  key: "username",
                  dropdown: true,
                  by_id: true
                },
                autosuggest_save: {
                  key: "tech_user_id"
                },
                editable: true,
                selfgenerated: true
              },
              {
                key: "garancija",
                text: "Vrsta naloga",
                multiple_occurence: true,
                first_occurance: true,
                input_type: "dropdown",
                autosuggest: {
                  table: "nalog",
                  key: "garancija",
                  dropdown: true,
                  by_enum: "true"
                },
                editable: true
              }
            ]
          },
          {
            data: [
              {
                key: "status", // OK
                text: "Status",
                input_type: "dropdown",
                selfgenerated: true,
                editable: false,
                autosuggest: {
                  table: "nalog",
                  key: "status",
                  dropdown: true,
                  by_enum: "true"
                }
              },
              {
                key: "nadaljevanje_dne", // se  izračuna!
                text: "Nadaljevanje dne",
                input_type: "date",
                selfgenerated: true,
                editable: false
              }
            ]
          },
          {
            data: [
              {
                key: "datum_in",
                text: "Z dne",
                input_type: "date",
                selfgenerated: true,
                editable: false
              },
              {
                key: "datum_out", // NAJDI
                text: "Končano dne",
                input_type: "date",
                selfgenerated: true,
                editable: false
              },
              {
                key: "prioriteta", // OK
                text: "Prioriteta",
                input_type: "dropdown",
                editable: false,
                selfgenerated: true,
                autosuggest: {
                  table: "nalog",
                  key: "prioriteta",
                  dropdown: true,
                  by_enum: "true"
                }
              }
            ]
          }
        ]
      }
    ]
  },
  podrobnosti: {
    logistika_vhod: {
      text: "Logistika vhod",
      new_ticket_info: true, // ALI JE PODATKE TREBA IZPOLNITI OB NEW TICKETU
      rows: [
        {
          columns: [
            {
              data: [
                {
                  key: "cvar_posiljatelj", // OK
                  text: "Pošiljatelj",
                  new_ticket_info: true,
                  input_type: "autosuggest",
                  add_new: true,
                  add_new_url: "../partner/dodajPartnerja.php?type='all'",
                  autosuggest: {
                    table: "partner",
                    key: "partner",
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "posiljatelj_id"
                  },
                  details: true
                },
                {
                  key: "posiljatelj_ticket", // OK
                  text: "Pošiljatelj ticket",
                  new_ticket_info: true,
                  input_type: "string"
                }
              ]
            },
            {
              data: [
                {
                  key: "cvar_dostava", // OK
                  text: "Dostava",
                  new_ticket_info: true,
                  add_new: true,
                  add_new_url: "../prevozniki/dodajPrevoznika.php",
                  input_type: "autosuggest",
                  autosuggest: {
                    table: "prevoznik",
                    key: "prevoznik",
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "kurir_in_id"
                  }
                },
                {
                  key: "tracking_in", // OK
                  text: "Tracking št",
                  new_ticket_info: true,
                  input_type: "string"
                }
              ]
            },
            {
              data: [
                {
                  key: "cvar_lastnik_opreme",
                  text: "Lastnik opreme",
                  new_ticket_info: true,
                  add_new: true,
                  add_new_url: "../partner/dodajPartnerja.php?type='all'",
                  input_type: "autosuggest",
                  autosuggest: {
                    table: "partner",
                    key: "partner",
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "lastnik_id"
                  }
                }/*,
                {                    // SE NI!
                  key: "racun",
                  text: "Račun",
                  new_ticket_info: true,
                  input_type: "file",
                  editable: true,
                  required: true
                }*/
              ]
            }
          ]
        }
      ]
    },
    oprema: {
      text: "Oprema",
      new_ticket_info: true,
      rows: [
        {
          columns: [
            {
              data: [
                {
                  key: "serijska", // OK
                  text: "Serijska št",
                  new_ticket_info: true,
                  input_type: "string",
                  required: true
                },
                {
                  key: "geslo_naprave", // OK
                  text: "Geslo",
                  new_ticket_info: true,
                  input_type: "string",
                  editable: true
                },
                {
                  key: "podatki_pomembni", // OK
                  text: "Podatki pomembni",
                  new_ticket_info: true,
                  input_type: "dropdown",
                  editable: true,
                  autosuggest: {
                    table: "nalog",
                    key: "podatki_pomembni",
                    dropdown: true,
                    by_enum: "true"
                  }
                },
                {
                  key: "dodatki", // OK
                  text: "Priloženi dodatki",
                  new_ticket_info: true,
                  input_type: "string",
                  editable: true
                },
                {
                  key: "embalaza", // OK
                  text: "Embalaža",
                  new_ticket_info: true,
                  input_type: "dropdown",
                  editable: true,
                  autosuggest: {
                    table: "nalog",
                    key: "embalaza",
                    dropdown: true,
                    by_enum: "true"
                  }
                },
                {
                  key: "stanje_opreme", // OK
                  text: "Stanje opreme",
                  new_ticket_info: true,
                  input_type: "string" // dropdown
                }
              ]
            },
            {
              data: [
                {
                  key: "oprema_id",
                  input_type: "tree-structure",
                  text: "Oprema",
                  new_ticket_info: true,
                  add_new: true,
                  add_new_url: "../oprema/dodajOpremo.php",
                  data: [
                    {
                      key: "cvar_proizvajalec", // vse tole in spodaj pride iz oprema_id
                      text: "Proizvajalec",
                      new_ticket_info: true,
                      input_type: "autosuggest",
                      custom_var: true,
                      by_id_key: "cvar_proizvajalec_id",
                      autosuggest: {
                        table: "proizvajalec",
                        key: "proizvajalec",
                        by_id: true,
                        minLength: 0,
                        tree_special: true,
                        tree_structure: true,
                        tree_table: "oprema",
                        tree_id: "proizvajalec_id"
                      }
                    },
                    {
                      key: "cvar_vrsta", // NAJDI
                      text: "Vrsta opreme",
                      new_ticket_info: true,
                      input_type: "autosuggest",
                      custom_var: true,
                      autosuggest: {
                        table: "oprema",
                        key: "vrsta",
                        minLength: 0,
                        tree_structure: true,
                        tree_id: "vrsta"
                      }
                    },
                    {
                      key: "cvar_skupina", // NAJDI
                      text: "Tip",
                      new_ticket_info: true,
                      input_type: "autosuggest",
                      custom_var: true,
                      autosuggest: {
                        table: "oprema",
                        key: "skupina",
                        minLength: 0,
                        tree_structure: true,
                        tree_id: "skupina"
                      }
                    },
                    {
                      key: "cvar_model", // NAJDI
                      text: "Model",
                      new_ticket_info: true,
                      input_type: "autosuggest",
                      custom_var: true,
                      autosuggest: {
                        table: "oprema",
                        key: "model",
                        minLength: 0,
                        tree_structure: true,
                        tree_id: "model"
                      }
                    }
                  ]
                },
                {
                  key: "proizvajalec_ticket",
                  text: "Vezni dokument",
                  new_ticket_info: true,
                  input_type: "string"
                }
              ]
            },
            {
              data: [
                {
                  key: "garancija", // PREVERI
                  text: "Plačnik",
                  multiple_occurence: true,
                  input_type: "dropdown",
                  editable: true,
                  autosuggest: {
                    table: "nalog",
                    key: "garancija",
                    dropdown: true,
                    by_enum: true
                  }
                },
                {
                  key: "cvar_oiw_partner_id", // VERJETNO OK
                  text: "OIW kontakt",
                  add_new: true,
                  add_new_url: "../partner/dodajPartnerja.php?type='all'",
                  input_type: "autosuggest",
                  autosuggest: {
                    table: "partner",
                    key: "partner",
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "oiw_partner_id"
                  },
                  editable: true,
                  details: true
                },
                {
                  key: "oiw_partner_ticket", // VERJETNO OK
                  text: "OIW dokument",
                  input_type: "file",
                  editable: true
                },
                {
                  key: "iw_status",
                  text: "IW status",
                  input_type: "string",
                  //editable: true
                  editable: false
                }/*,
                {
                  key: "sla", // OK
                  text: "SLA",
                  new_ticket_info: true,
                  input_type: "string", // bo dropdown ampak nimamo nič tle!
                  editable: true
                }*/
              ]
            }
          ]
        }
      ]
    },
    servis: {
      text: "Servis",
      new_ticket_info: true,
      rows: [
        {
          columns: [
            {
              data: [
                {
                  key: "opis_napake", // OK
                  text: "Opis napake",
                  new_ticket_info: true,
                  input_type: "text",
                  required: true
                },
                {
                  key: "rezultat_diagnostike", // OK
                  text: "Diagnostika",
                  input_type: "text",
                  editable: true
                }
              ]
            }
          ]
        },
        {
          columns: [
            {
              data: [
                {
                  key: "workflow", // OK
                  text: "Izbira naloga (workflow)",
                  input_type: "string", // bo dropdown ampak nimamo nič tle
                  //editable: true
                  editable: false
                }
              ]
            },
            {
              data: [
                {
                  key: "odobreno", // OK
                  text: "Odobreno",
                  input_type: "dropdown",
                  autosuggest: {
                    table: "nalog",
                    key: "odobreno",
                    dropdown: true,
                    by_enum: "true"
                  }
                },
                {
                  key: "odobreno_date", // OK
                  text: "Odobreno dne",
                  input_type: "date"
                },
                {
                  key: "odobril_kontakt_id", // OK
                  text: "Odobril",
                  input_type: "string"
                }
              ]
            },
            {
              data: [
                {
                  key: "cvar_odg_tehnik", // NAJDI
                  text: "Nalog izvedel",
                  multiple_occurence: true,
                  input_type: "dropdown",
                  autosuggest: {
                    table: "users",
                    key: "username",
                    by_id: true,
                    dropdown: true
                  },
                  autosuggest_save: {
                    key: "tech_user_id"
                  },
                  editable: true,
                  selfgenerated: true
                }
              ]
            }
          ]
        },
        {
          columns: [
            {data: [{
                key: "kosovnica_count", // pride iz nalog_art_id
                text: "Kosovnica",
                input_type: "kosovnica",
                editable: true,
                table: {
                  table_id: "kosovnica",
                  table_id_key: "id_kosovnica",
                  data: [
                    {
                      key: "cvar_pn",
                      text: "Part number",
                      input_type: "autosuggest",
                      add_new: true,
                      add_new_url: "../artikel/dodajArtikel.php",
                      add_new_modal_id: "modal-second",
                      autosuggest: {
                        table: "artikel",
                        key: "pn",
                        by_id: true
                      },
                      autosuggest_save: {
                        key: "artikel_id"
                      },
                      required: true
                    },
                    {
                      key: "kolicina_kosovnica",
                      text: "Ocenjena poraba",
                      input_type: "integer",
                      required: true,
                      editable: false,
                      minVal: 1
                    },
                    {
                      key: "porabljeno_kosovnica",
                      text: "Porabljeno",
                      input_type: "integer",
                      editable: true,
                      minVal: 0
                    },
                    {
                      key: "cvar_cena",
                      text: "Cena/kos",
                      input_type: "string",
                      editable: false
                    }
                  ]
                }
              }]
            }
          ]
        }
      ]
    },
    logistika_izhod: {
      text: "Logistika izhod",
      rows: [
        {
          columns: [
            {
              data: [
                {
                  key: "datum_out", // SE PRIDE
                  text: "Obvestilo o zaključku dne",
                  input_type: "date",
                  selfgenerated: true,
                  editable: false
                }
              ]
            },
            {
              data: [
                {
                  key: "mail_id", // SE PRIDE
                  text: "Način obveščanja",
                  input_type: "string" // dropdown
                }
              ]
            },
            {
              data: [
                {
                  key: "cvar_user", // PREVERI
                  text: "User",
                  input_type: "dropdown",
                  selfgenerated: true,
                  editable: false,
                  autosuggest: {
                    table: "users",
                    key: "username",
                    dropdown: true,
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "sales_user_id"
                  }
                }
              ]
            }
          ]
        },
        {
          columns: [
            {
              data: [
                {
                  key: "cvar_prejemnik", // OK
                  text: "Prejemnik",
                  add_new: true,
                  add_new_url: "../partner/dodajPartnerja.php?type='all'",
                  input_type: "autosuggest",
                  autosuggest: {
                    table: "partner",
                    key: "partner",
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "prejemnik_id"
                  },
                  details: true
                }
              ]
            },
            {
              data: [
                {
                  key: "cvar_odprema", // OK
                  text: "Odprema",
                  add_new: true,
                  add_new_url: "../prevozniki/dodajPrevoznika.php",
                  input_type: "autosuggest",
                  autosuggest: {
                    table: "prevoznik",
                    key: "prevoznik",
                    by_id: true
                  },
                  autosuggest_save: {
                    key: "kurir_out_id"
                  }
                },
                {
                  key: "tracking_out", // OK
                  text: "Tracking št",
                  input_type: "string"
                }
              ]
            },
            {
              data: [
                {
                  key: "placnik_id", // SE PRIDE
                  text: "Plačnik",
                  /*input_type: "autosuggest",
                  autosuggest: {
                    table: "partner",
                    key: "partner",
                    by_id: true
                  }*/
                  input_type: "string",
                  editable: false
                },
                {
                  key: "st_racuna", // OK
                  text: "Št. računa",
                  input_type: "string"
                }
              ]
            }
          ]
        }
      ]
    }
  }
}
