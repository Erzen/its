function showTableNalogi(global_search) {

  loadFilters(function() {
    $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

    let tableFilters = document.createElement("div");
    $(".table-filters-wrapper").append(tableFilters);

    let filtersAdd = document.createElement("div");
    $(tableFilters).append(filtersAdd);

    let filtersParams = document.createElement("div");
    $(filtersParams).attr("id","table-filter-parameters")
    $(tableFilters).append(filtersParams);

    if (global_search) {
      $(".filter-container-col-three").append(addSearchParameter({key: "global_search", text: "Iskalnik orodne vrstice", input_type: "string"},{value: global_search},false));
    }

    let table = document.createElement("table");
    $(table).addClass("table table-striped wrap dataTable dtr-inline collapsed");
    $(table).attr("id","table-"+globals.variable_settings.key);
    $(".table-wrapper").append(table);

    var tableContent = $(table).DataTable({
      serverSide: true,
      ajax: {
        url: globals.variable_settings.api_url,
        type: 'POST',
        data: function ( d ) {
          d.table = globals.variable_settings.key
          d.key_id= globals.variable_settings.id_key
          d.filters= readFilters()
        }
      },
      language: {
        emptyTable: "Brez nalogov."
      },
      dom: "<'row'<'col-md-6'l><'col-md-6'B>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
      searching: false,
      columns: [
        {
          data: null,
          title: "",
          className: "all",
          fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("");

            let actions = document.createElement("div");
            $(actions).addClass("dropdown");
            $(actions).attr("style","margin-left:10px; float: right; display: inline-block;");
            $(nTd).append(actions);

            let i = document.createElement("i");
            $(i).addClass("fas fa-cog dropdown-toggle").attr("data-toggle","dropdown").attr("aria-haspopup","true").attr("aria-expanded","false");
            $(actions).append(i);

            let actionsListDiv = document.createElement("div");
            $(actionsListDiv).addClass("dropdown-menu dropdown-menu-right");
            $(actions).append(actionsListDiv);

            let aUredi = document.createElement("a");
            $(aUredi).addClass("dropdown-item").attr("href","#");
            $(aUredi).append("Uredi");
            $(aUredi).click(function() {
              globals.functions.urediNalog(iRow);
            });
            $(actionsListDiv).append(aUredi);

            let aIzbrisi = document.createElement("a");
            $(aIzbrisi).addClass("dropdown-item").attr("href","#");
            $(aIzbrisi).append("Izbriši");
            $(aIzbrisi).click((function(table) {
              return function() {
                if (confirm("Ali želiš izbrisati nalog?") == true) {
                  let params = {};
                  let id_key = globals.variable_settings.id_key;
                  params.id_key = id_key;
                  params.id_value = oData[id_key];
                  params.table = globals.variable_settings.key;
                  Ajax(globals.variable_settings.remove_url,params, function() {
                    $(table).DataTable().ajax.reload(null,false);
                  });
                }
              }
            })(table));
            $(actionsListDiv).append(aIzbrisi);

            let glavne_info = {};
            for (let i=0; i<globals.variable_settings.glavne_info.rows.length; i++) {
              for (let j=0; j<globals.variable_settings.glavne_info.rows[i].columns.length; j++) {
                for (let k=0; k<globals.variable_settings.glavne_info.rows[i].columns[j].data.length; k++) {
                  let key = globals.variable_settings.glavne_info.rows[i].columns[j].data[k].key;
                  glavne_info[key] = sData[key];
                  if (globals.variable_settings.glavne_info.rows[i].columns[j].data[k].autosuggest && globals.variable_settings.glavne_info.rows[i].columns[j].data[k].autosuggest.by_id) {
                    let key2 = globals.variable_settings.glavne_info.rows[i].columns[j].data[k].autosuggest_save.key;
                    glavne_info[key2] = sData[key2];
                  }
                }
              }
            }

            glavne_info[globals.variable_settings.nalog_id_key] = sData[globals.variable_settings.nalog_id_key];

            fillDetails(nTd,glavne_info,globals.variable_settings.glavne_info,"table");


          }
        },
        {
          data: null,
          title: "podrobnosti",
          className: "none"
        }
      ],
      buttons: [
        {
          text: '<i class="fas fa-plus-circle"></i> Dodaj nalog',
          action: function ( e, dt, node, config ) {
            $("#modal").modal("show");
            globals.functions.dodajNalog();
          },
          init: function(api, node, config) {
            $(node).removeClass('dt-button')
          },
          className: "btn btn-custom"
        }
      ],
      //order: [[ 2, 'desc' ]],
      responsive: {
        details: {
          renderer: function ( api, rowIdx, columns ) {
            data = columns[1].data

            let wrapperDiv = document.createElement("div");
            $(wrapperDiv).addClass("slider");
            $(wrapperDiv).css("width","100%")

            for (var key in globals.variable_settings.podrobnosti) {

              let list_item_div = document.createElement("div");
              $(wrapperDiv).append(list_item_div)

              let a = document.createElement("a");
              $(a).addClass("list-item");
              $(a).attr("href","#details-"+key).attr("data-toggle","collapse");
              $(a).append("<i class='fas fa-chevron-right i-fas-chevron'></i>"+globals.variable_settings.podrobnosti[key].text);
              $(a).click(function() {
                $(this).toggleClass("active");
                $(this).find("i.fas.i-fas-chevron").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
              });
              $(list_item_div).append(a);
              let aEdit = document.createElement("i");
              $(aEdit).addClass("fas fa-pencil-alt icon-link edit-link");
              $(a).append(aEdit);
              $(aEdit).click((function(key,rowIdx) {
                return function(event) {
                  event.stopPropagation();
                  let editCategories = [];
                  editCategories.push(key);
                  globals.functions.urediNalog(rowIdx,editCategories);
                }
              })(key,rowIdx))

              let div = document.createElement("div");
              $(div).addClass("collapse list-item-content");
              $(div).attr("id","details-"+key);

              fillDetails(div,data,globals.variable_settings.podrobnosti[key],"table")

              $(wrapperDiv).append(div);
            }

            return wrapperDiv
          },
          display: function(row, update, render) {
            if (update) {
              if ($(row.node()).hasClass('parent')) {
                row.child(render(), 'child').show();
                $('div.slider', row.child()).slideDown(0);
                return true;
              }
            }
            else {
              if (!row.child.isShown()) {
                row.child(render(), 'child').show();
                $(row.node()).addClass('parent shown');
                $('div.slider', row.child()).slideDown();
                return true;
              }
              else {
                $('div.slider', row.child()).slideUp(function() {
                  row.child(false);
                  $(row.node()).removeClass('parent shown');
                });

                return false;
              }
            }
          }
        }
      },
      createdRow: function ( row, data, index ) {

        $(row).click( function() {

          var openRow = tableContent.row('.parent');
          if (openRow[0].length == 0 || openRow[0][0] != tableContent.row(this)[0][0]) {
            $(openRow.node()).removeClass("parent");
            openRow.child.hide();
          }

          var isClicked = !($(row).hasClass("parent"));
        })
      }
    })
  });
}
