let defaultFilterSettings = { // to bo prišlo iz baze
  order: {value: "id_ticket", dir: "asc"},
  search: {
    cvar_odg_tehnik: {value: "nikae", id: "10"},
    cvar_posiljatelj: {value: "ITS Intertrade d.o.o.", id: "1"} // v bazi bo z id-ji, ki jih potem backend pretvori v text
  }
}

function loadFilters(cb) {
  //$(".table-filters-wrapper").html("");

  let filterContainer = document.createElement("div");
  $(filterContainer).addClass("container-fluid filter-container");
  $(".table-filters-wrapper").append(filterContainer);

  let row = document.createElement("div");
  $(row).addClass("row");
  $(filterContainer).append(row);

  let colOne = document.createElement("div");
  $(colOne).addClass("col filter-container-col-one");
  $(row).append(colOne);

  let colTwo = document.createElement("div");
  $(colTwo).addClass("col filter-container-col-two");
  $(row).append(colTwo);

  let colThree = document.createElement("div");
  $(colThree).addClass("col filter-container-col-three");
  $(row).append(colThree);

  let colFour = document.createElement("div");
  $(colFour).addClass("col filter-container-col-four");
  $(row).append(colFour);

  $(colOne).append(filterInfoDiv());

  $(colTwo).append("<span class='bold-text'>Razvrsti po:</span>")
  $(colTwo).append(generateSortBy());

  $(colThree).append("<span class='bold-text'>Išči po:</span>")
  $(colThree).append(generateSearchBy());

  /*$(colFour).append("<span class='bold-text'>Globalni filtri:</span>")*/

  let rowTwo = document.createElement("div");
  $(rowTwo).addClass("row justify-content-end");
  $(filterContainer).append(rowTwo);

  setTimeout(function() {
    cb();
  },200) // VPRAŠANJE ALI BO TO ZADOSTOVALO!
}

function getColumnsList() {
  let columnsList = getColumnsListPartial(globals.variable_settings.glavne_info);

  for (let key in globals.variable_settings.podrobnosti) {
    let podrobnosti = getColumnsListPartial(globals.variable_settings.podrobnosti[key]);
    columnsList = columnsList.concat(podrobnosti);
  }


  return columnsList;
}

function getColumnsListPartial(data) {
  let columnsList = [];
  for (let i=0; i<data.rows.length; i++) {
    for (let j=0; j<data.rows[i].columns.length; j++) {
      for (let k=0; k<data.rows[i].columns[j].data.length; k++) {
        let columnData = data.rows[i].columns[j].data[k];
        let addToList = false;
        if (columnData.multiple_occurence) {
          if (columnData.first_occurance) {
            addToList = true;
          }
        }
        else {
          addToList = true;
        }

        if (addToList) {
          if (columnData.input_type == "file") {

          }
          else if (columnData.input_type == "tree-structure") {
            for (l=0; l<columnData.data.length; l++) {
              columnsList.push({id: columnData.data[l].key, value: columnData.data[l].text+" ("+columnData.text+")"})
            }
          }
          else {
            columnsList.push({id: columnData.key, value: columnData.text})
          }
        }
      }
    }
  }

  return columnsList;
}

function filterInfoDiv() {
  let div = document.createElement("div");
  $(div).append("<h5>Filtri</h5>")

  let runFilters = document.createElement("button");
  $(runFilters).addClass("btn btn-custom-outline");
  $(runFilters).append("Osveži tabelo");
  $(runFilters).click(function() {
    $("#table-nalog").DataTable().ajax.reload(null);
  });
  $(div).append(runFilters)

  let defaultFilters = document.createElement("button");
  $(defaultFilters).addClass("btn btn-custom-outline");
  $(defaultFilters).append("Ponastavi filtre");
  $(defaultFilters).click(function() {
    Ajax(globals.variable_settings.session_params_url, {key: "global_search"}, function(result) {
      $(".table-filters-wrapper").html("");
      loadFilters();
      $("#table-nalog").DataTable().ajax.reload(null);
    });
  });
  $(div).append(defaultFilters)

  return div;
}

function addSearchParameter(options,value,editable) {

  let input, inputGroup

  if (options.key != "global_search") {
    input = formFiller(options,options.input_type,{value: value, editable: editable, filterDiv: true})
  }
  else {
    input = formFiller(options,options.input_type,{value: value.value, editable: editable, filterDiv: true})
  }


  if (["select","input"].indexOf(input.nodeName.toLowerCase()) != -1) {
    inputGroup = document.createElement("div");
    $(inputGroup).addClass("input-group mb-3 query-input ui-widget filter-container-filter");

    if (editable != true) {
      $(input).attr("disabled","true")
    }
    $(input).removeClass("my-1 mr-sm-2")
    $(inputGroup).append(input);

    $(input).addClass("form-control");
    $(input).attr("id","table-filters-"+options.key);
    //$(input).val(value.id || value.value);
    //$(input).html(value.value);
    $(inputGroup).append(input);
  }
  else {
    inputGroup = input;
    $(inputGroup).removeClass("form-group").addClass("input-group filter-container-filter");
  }

  let inputPrependex = document.createElement("div");
  $(inputPrependex).addClass("input-group-prepend");
  $(inputPrependex).append('<span class="input-group-text">'+options.text+'</span>');
  $(inputGroup).prepend(inputPrependex)

  let removeButton = document.createElement("div");
  $(removeButton).addClass("input-group-append");
  $(removeButton).append('<a href="#" class="input-group-text"><i class="fas fa-times"></i></a>');
  $(removeButton).click(function() {
    $(inputGroup).remove();
    if (options.key == "global_search") {
      Ajax(globals.variable_settings.session_params_url, {key: "global_search"}, function(result) {});
    }
    $("#table-nalog").DataTable().ajax.reload(null);
  })
  $(inputGroup).append(removeButton)


  /*let inputGroup = document.createElement("div");
  $(inputGroup).addClass("input-group mb-3 query-input ui-widget filter-container-sort");
  let inputPrependex = document.createElement("div");
  $(inputPrependex).addClass("input-group-prepend");
  $(inputPrependex).append('<span class="input-group-text">'+options.text+'</span>');
  $(inputGroup).append(inputPrependex)
  let input = document.createElement("input");
  if (editable != true) {
    $(input).attr("disabled","true")
  }
  $(input).addClass("form-control");
  $(input).attr("id","table-filters-"+options.key);
  $(input).val(value.id || value.value);
  $(input).html(value.value);
  $(inputGroup).append(input);
  let removeButton = document.createElement("div");
  $(removeButton).addClass("input-group-append");
  $(removeButton).append('<a href="#" class="input-group-text"><i class="fas fa-times"></i></a>');
  $(removeButton).click(function() {
    $(inputGroup).remove();
    if (options.key == "global_search") {
      Ajax(globals.variable_settings.session_params_url, {key: "global_search"}, function(result) {});
    }
    $("#table-nalog").DataTable().ajax.reload(null);
  })
  $(inputGroup).append(removeButton)*/

  return inputGroup;
}

function generateSortBy() {
  let availableColumns = getColumnsList();

  let inputGroup = document.createElement("div");
  $(inputGroup).addClass("input-group mb-3 query-input ui-widget filter-container-sort");
  let input = document.createElement("select");
  $(input).addClass("custom-select");
  $(inputGroup).append(input);
  let sortDir = document.createElement("div");
  $(sortDir).addClass("input-group-append sort-"+defaultFilterSettings.order.dir);
  $(sortDir).append('<a href="#" class="input-group-text" id="nalogi-table-filters-sorting"></a>');
  $(sortDir).click(function() {
    if ($(this).hasClass("sort-asc")) {
      $(this).removeClass("sort-asc").addClass("sort-desc");
    }
    else {
      $(this).removeClass("sort-desc").addClass("sort-asc");
    }
  })
  $(inputGroup).append(sortDir)

  for (let i=0; i<availableColumns.length; i++) {
    let option = document.createElement("option");
    if (defaultFilterSettings.order.value == availableColumns[i].id) {
      $(inputGroup).val(availableColumns[i].id);
      $(option).attr("selected",true);
    }
    $(option).attr("value",availableColumns[i].id);
    $(option).append(availableColumns[i].value);
    $(input).append(option);
  }

  return inputGroup;
}

function generateSearchBy() {

  let searchParameters = defaultFilterSettings.search;

  for (let key in searchParameters) {

    let options = getOptionsFromSettings(key);
    $(".filter-container-col-three").append(addSearchParameter(options, searchParameters[key],true));
  }
}

function readFilters() {

  let filters = {};

  let sortInput = $(".filter-container-sort");
  let sortId = $(sortInput).find("select").val() || "id_ticket";
  let sortDir = $(sortInput).find(".input-group-append").hasClass("sort-asc") ? "asc" : "desc";

  filters.order = {column: sortId, dir: sortDir};
  filters.search = {}

  let filtersInputGroups = $(".filter-container-col-three").find(".input-group.filter-container-filter");

  for (let i=0; i<filtersInputGroups.length; i++) {
    let select = $(filtersInputGroups[i]).find("select");
    let input = $(filtersInputGroups[i]).find("input");
    input =  select.length == 0 ? input : select;

    let key = $(input).attr("id").replace("table-filters-","");
    if (key == "global_search") { // globalni filter!
      filters.global_search = $("#table-filters-global_search").val();
    }
    else {
      let options = getOptionsFromSettings(key);
      let value;
      if (options.input_type == "autosuggest") {
        value = $(input).data("uiAutocomplete").selectedItem;
        if (value == null) {
          value = undefined;
        }
        else {
          if (options.autosuggest.by_id) {
            value = $(input).data("uiAutocomplete").selectedItem.id;
          }
          else {
            value = $(input).data("uiAutocomplete").selectedItem.value;
          }
        }
      }
      else if (options.input_type == "date") {
        if ($(input).val() != "") {
          value = $(input).val().split("/");
          let yy = value[2];
          let mm = value[1];
          let dd = value[0];
          let today = new Date();
          value = yy+"-"+mm+"-"+dd+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
        }
      }
      else {
        if ($(input).val() != "") {
          value = $(input).val();
        }
      }
    }
  }

  return filters;
}

function getOptionsFromSettings(key) {

  let options = getOptionsFromSettingsPartial(globals.variable_settings.glavne_info,key);

  for (let key2 in globals.variable_settings.podrobnosti) {
    if (options == false) {
      options = getOptionsFromSettingsPartial(globals.variable_settings.podrobnosti[key2],key);
    }
  }

  return options;
}

function getOptionsFromSettingsPartial(data,key) {
  let columnsList = [];
  for (let i=0; i<data.rows.length; i++) {
    for (let j=0; j<data.rows[i].columns.length; j++) {
      for (let k=0; k<data.rows[i].columns[j].data.length; k++) {
        let columnData = data.rows[i].columns[j].data[k];
        if (columnData.input_type == "tree-structure") {
          for (l=0; l<columnData.data.length; l++) {
            if (columnData.data[l].key == key) {
              return columnData.data[l];
            }
          }
        }
        else if (columnData.key == key) {
          return columnData;
        }
      }
    }
  }

  return false;
}
