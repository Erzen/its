globals.functions = {
  dodajNalog: function() {

    openModal("modal",{clear: true, size: "xl", title: '<i class="fas fa-plus-circle"></i> Dodaj nalog'});

    let wrapperDiv = document.createElement("div");
    $(wrapperDiv).css("width","100%");
    $("#modal .modal-body").append(wrapperDiv)

    if (globals.variable_settings.glavne_info.new_ticket_info) {
      let a = document.createElement("a");
      $(a).addClass("list-item active");
      $(a).attr("href","#details-modal-glavne_info").attr("data-toggle","collapse");
      $(a).append("<i class='fas fa-chevron-down i-fas-chevron'></i>"+globals.variable_settings.glavne_info.text);
      $(a).click(function() {
        $(this).toggleClass("active");
        $(this).find("i.fas.i-fas-chevron").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
      })
      $(wrapperDiv).append(a);

      let div = document.createElement("div");
      $(div).addClass("collapse list-item-content show");
      $(div).attr("id","details-modal-glavne_info");

      fillDetails(div,null,globals.variable_settings.glavne_info,"new");

      $(wrapperDiv).append(div);

    }

    for (var key in globals.variable_settings.podrobnosti) {
      if (globals.variable_settings.podrobnosti[key].new_ticket_info) {

        let a = document.createElement("a");
        $(a).addClass("list-item active");
        $(a).attr("href","#details-modal-"+key).attr("data-toggle","collapse");
        $(a).append("<i class='fas fa-chevron-down i-fas-chevron'></i>"+globals.variable_settings.podrobnosti[key].text);
        $(a).click(function() {
          $(this).toggleClass("active");
          $(this).find("i.fas.i-fas-chevron").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
        })
        $(wrapperDiv).append(a);

        let div = document.createElement("div");
        $(div).addClass("collapse list-item-content show");
        $(div).attr("id","details-modal-"+key);

        fillDetails(div,null,globals.variable_settings.podrobnosti[key],"new")

        $(wrapperDiv).append(div);
      }
    }

    let buttonSave = document.createElement("button");
    $(buttonSave).addClass("btn btn-custom");
    $(buttonSave).append("Shrani");
    $(buttonSave).click(function() {
      let div = $("#modal .modal-body");
      globals.functions.preberiNalog(div, function(nalog) {
        if ($(div).find(".missing-value").length == 0) {
          setWorkflowStatus(nalog,{type: "new"}, function(nalog) {
            if (nalog) {
              console.log("sending nalog")
              Ajax(globals.variable_settings.create_url,{table: globals.variable_settings.key, data: nalog}, function(results) {
                $("#modal").modal("hide");
                $("#table-nalog").DataTable().ajax.reload(null,true);
              });
            }
          });
        }
      });
    })

    $("#modal .modal-footer").html("").append(buttonSave);
  },
  urediNalog: function(rowIndex,editCategories) {

    let rowInfo = $("#table-nalog").DataTable().rows(rowIndex).data()[0];

    let title = rowInfo.id_ticket+" ("+(rowInfo.cvar_lastnik_opreme || rowInfo.cvar_posiljatelj)+")"

    openModal("modal",{clear: true, size: "xl", title: '<i class="fas fa-pencil-alt"></i> Uredi nalog '+title});

    let defaultCategories = Object.keys(globals.variable_settings.podrobnosti);
    defaultCategories.unshift("glavne_info");

    editCategories = editCategories || defaultCategories

    let wrapperDiv = document.createElement("div");
    $(wrapperDiv).css("width","100%");
    $("#modal .modal-body").append(wrapperDiv)

    for (var i=0; i<editCategories.length; i++) {
      let key = editCategories[i];

      let settings = (key == "glavne_info") ? globals.variable_settings.glavne_info : globals.variable_settings.podrobnosti[key];

      let a = document.createElement("a");
      $(a).addClass("list-item active");
      $(a).attr("href","#details-modal-"+key).attr("data-toggle","collapse");
      $(a).append("<i class='fas fa-chevron-down i-fas-chevron'></i>"+settings.text);
      $(a).click(function() {
        $(this).toggleClass("active");
        $(this).find("i.fas.i-fas-chevron").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
      })
      $(wrapperDiv).append(a);

      let div = document.createElement("div");
      $(div).addClass("collapse list-item-content show");
      $(div).attr("id","details-modal-"+key);
      fillDetails(div,rowInfo,settings,"edit")

      $(wrapperDiv).append(div);
    }

    let buttonSave = document.createElement("button");
    $(buttonSave).addClass("btn btn-custom");
    $(buttonSave).append("Shrani");
    $(buttonSave).click(function() {
      let div = $("#modal .modal-body");
      let nalog = globals.functions.preberiNalog(div, function(nalog) {
        if ($(div).find(".missing-value").length == 0) {
          setWorkflowStatus(nalog,{details: rowInfo}, function(nalog) {
            if (nalog) {
              console.log("sending nalog")
              let id = rowInfo[globals.variable_settings.nalog_id_key];
              Ajax(globals.variable_settings.update_url,{table: globals.variable_settings.key, data: nalog, id_key: globals.variable_settings.nalog_id_key, id_value: id}, function(results) {
                $("#modal").modal("hide");
                $("#table-nalog").DataTable().ajax.reload(null,true);
              });
            }
          });
        }
      });
    })

    $("#modal .modal-footer").html("").append(buttonSave);
  },
  preberiNalog: function(div,cb) {

    let nalog = {}

    readSection(div,globals.variable_settings.glavne_info, function(key,value) {
      nalog[key] = value;
    });

    for (var key in globals.variable_settings.podrobnosti) {
      readSection(div,globals.variable_settings.podrobnosti[key], function(key,value) {
        nalog[key] = value;
      });
    }

    cb(nalog);
  }
}
