/*statusi:
- čaka diagnostiko 0
- čaka izbor garancije 1
- DOA, CB, stvarna napaka 2 (a to so 3 različni statusi?)
- redno popravilo 3
- čakamo odobritev garancije 4
- čakamo odobritev stranke 5
- čakamo kosovnico 6
- popravilo se izvaja 7
- čakamo rezervne dele 8
- končano 9
- vračilo proizvajalcu 10
- zaključeno 11

prioritete:
- normal 0
- high 1

sla:
- 14dni 0*/

function setWorkflowStatus(nalog,options,cb) {

  $("#modal .modal-body input").removeClass("custom-alert-input");

  let kosovnica_narejena = false;
  let garancija_odobrena = false;
  let naroci_dele = false;
  let popravilo_koncano = false;
  let nalog_zakljucen = false;
  let predvideni_dodatni_stroski = false;

  let new_nalog = {};
  if (options.type == "new") {
    new_nalog = nalog;
    new_nalog.sla = 0;

    if (new_nalog.posiljatelj_id == undefined && new_nalog.lastnik_id == undefined) {
      alert("Izpolni vsaj enega od polj Pošiljatelj in Lastnik opreme!");
      $("#custom-input-id-cvar_posiljatelj, #custom-input-id-cvar_lastnik_opreme").addClass("custom-alert-input");
      cb(false);
      return;
    }

    let today = new Date();
    let delayed_day = new Date();
    delayed_day = getDateAsJson(delayed_day,1);
    today = getDateAsJson(today);

    new_nalog.datum_in = today.yy+"-"+today.mm+"-"+today.dd+" "+today.hh+":"+today.min+":"+today.ss;
    new_nalog.prioriteta = 0;
    new_nalog.status = 0;
    new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;

    cb(new_nalog);
    return;
  }
  else if (options.details) {
    for (let key in nalog) {
      if (nalog[key] != options.details[key]) {
        new_nalog[key] = nalog[key];
      }
    }

    let diagnostika_changed = new_nalog.rezultat_diagnostike != undefined;
    let garancija_changed = new_nalog.garancija != undefined;
    let garancija_odobrena = new_nalog.odobreno != undefined;
    let garancija_odobrena_kontakt = new_nalog.odobril_kontakt_id != undefined;

    if (diagnostika_changed) {
      let today = new Date();
      delayed_day = getDateAsJson(today,4,"hour");
      new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
      new_nalog.status = 1;
      // določi tech_userja v php
    }
    /*if (garancija_changed) {
      if (new_nalog.garancija == "Preverjamo") {
        let today = new Date();
        let delayed_day = getDateAsJson(today,3);
        new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
        new_nalog.status = 4;
      }
      else if (new_nalog.garancija == "IOW" || new_nalog.garancija == "OOW") {
        new_nalog.status = 6;
        let today = new Date();
        let delayed_day = getDateAsJson(today,1);
        new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
      }
      else if (new_nalog.garancija == "IW") {
        // nujno določi iw_status
        if (nalog.iw_status == undefined) {
          alert("Izpolni polje iw status v Opremi!");
          cb(false);
          return;
        }
        if (nalog.iw_status = "Redno popravilo") {
          new_nalog.status = 6;
          let today = new Date();
          let delayed_day = getDateAsJson(today,1);
          new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
        }
        else {
          let today = new Date();
          let delayed_day = getDateAsJson(today,7);
          new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
          new_nalog.status = 4;
          new_nalog.prioriteta = 0;
        }
      }
    }
    if (nalog.garancija == "IW" && nalog.iw_status && nalog.iw_status != "Redno popravilo" && ((garancija_odobrena && !(garancija_odobrena_kontakt)) || (!(garancija_odobrena) && garancija_odobrena_kontakt))) {
      alert("Polji Odobreno in Odbril morata biti izpolnjeni.")
      cb(false);
      return;
    }
    else if (nalog.garancija == "IW" && nalog.iw_status && nalog.iw_status != "Redno popravilo" && garancija_odobrena && garancija_odobrena_kontakt) {
      if (new_nalog.odobreno_date == undefined) {
        let today = new Date();
        today = getDateAsJson(today);
        new_nalog.odobreno_date = today.yy+"-"+today.mm+"-"+today.dd+" "+today.hh+":"+today.min+":"+today.ss;
      }

      if (garancija_odobrena == "DA") {
        // vrniti opremo proizvajalcu
        new_nalog.status = 10;
        // time limit je do konca sla!
      }
      else if (garancija_odobrena == "NE") {
        new_nalog.status = 6;
        let today = new Date();
        let delayed_day = getDateAsJson(today,1);
        new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
      }
    }
    if (kosovnica_narejena) {
      // preveri in morebiti zamenjaj userja!
      if (predvideni_dodatni_stroski) {
        if (predvideni_dodatni_stroski == "DA") {
          // vprašati stranko
        }
        else if (predvideni_dodatni_stroski == "NE") {
          // status preveri dele za naročilo
        }
      }
      else {
        alert("Ali so predvideni dodatni stroški?!")
        cb(false);
        return;
      }
    }
    if (naroci_dele) {
      if (naroci_dele == "DA") {
        // status cakamo rezervne dele
      }
      else if (naroci_dele == "NE") {
        // status popravilo se izvaja
        let today = new Date();
        let delayed_day = getDateAsJson(today,1);
        new_nalog.nadaljevanje_dne = delayed_day.yy+"-"+delayed_day.mm+"-"+delayed_day.dd+" "+delayed_day.hh+":"+delayed_day.min+":"+delayed_day.ss;
      }
    }
    if (popravilo_koncano) {
      new_nalog.status = 9;
    }
    if (nalog_zakljucen) {
      // preberi userja ki je zakljucil nalog
      let today = new Date();
      today = getDateAsJson(today);
      new_nalog.datum_out = today.yy+"-"+today.mm+"-"+today.dd+" "+today.hh+":"+today.min+":"+today.ss;
      new_nalog.status = 11;
      // ko je nalog zaključen daj v arhiv!
    }*/
    cb(new_nalog);
    return;
  }
}
