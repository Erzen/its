<?
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  };
?>

<!DOCTYPE html>

<?php
include ('../users/auth.php');
?>

<html>
<?php
include ('../header.php');
?>

<body onLoad="showTableNalogi('<?php echo isset($_SESSION['global_search']) ? $_SESSION['global_search'] : ''?>');">
  <?php  include ('../navigacija.php'); ?>

  <main role="main">
    <div class="table-filters-wrapper">
    </div>
      <div class="table-wrapper">
      </div>
    </main>

  <div id="modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <div id="modal-second" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <!-- include custom index.js -->
  <link rel="stylesheet" href="custom/css/style.css">
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="custom/js/settings.js"></script>
  <script type="text/javascript" src="custom/js/functions.js"></script>
  <script type="text/javascript" src="custom/js/tableFilters.js"></script>
  <script type="text/javascript" src="custom/js/table.js"></script>
  <script type="text/javascript" src="custom/js/workflow.js"></script>

</body>

</html>
