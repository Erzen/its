globals.variable_settings = {
  urls: {
    api_url: "../ajax_simple_table/retrieve.php",
    create_url: "../ajax_simple_table/create.php",
    remove_url: "../ajax_simple_table/remove.php",
    get_selected_url: "../ajax_simple_table/getSelected.php",
    update_url: "../ajax_simple_table/update.php"
  },
  texts: {
    create: "Dodaj artikel",
    edit: "Uredi artikel"
  },
  key: "artikel",
  id_key: "id_artikel",
  text: "Osnovni podatki",
  data: [
    {
      key: "oprema_id",
      table_view_key: "oprema",
      input_type: "tree-structure",
      new_ticket_info: true,
      required: true,
      add_new: true,
      add_new_url: "../oprema/dodajOpremo.php",
      text: "Oprema",
      data: [
        {
          key: "cvar_proizvajalec",
          text: "Proizvajalec",
          new_ticket_info: true,
          input_type: "autosuggest",
          editable: true,
          custom_var: true,
          by_id_key: "cvar_proizvajalec_id",
          autosuggest: {
            table: "proizvajalec",
            key: "proizvajalec",
            by_id: true,
            minLength: 0,
            tree_special: true,
            tree_structure: true,
            tree_table: "oprema",
            tree_id: "proizvajalec_id"
          }
        },
        {
          key: "cvar_vrsta", // NAJDI
          text: "Vrsta opreme",
          new_ticket_info: true,
          input_type: "autosuggest",
          custom_var: true,
          editable: true,
          autosuggest: {
            table: "oprema",
            key: "vrsta",
            minLength: 0,
            tree_structure: true,
            tree_id: "vrsta"
          }
        },
        {
          key: "cvar_model", // NAJDI
          text: "Model",
          new_ticket_info: true,
          input_type: "autosuggest",
          custom_var: true,
          editable: true,
          autosuggest: {
            table: "oprema",
            key: "model",
            minLength: 0,
            tree_structure: true,
            tree_id: "model"
          }
        },
        {
          key: "cvar_skupina", // NAJDI
          text: "Tip",
          new_ticket_info: true,
          input_type: "autosuggest",
          custom_var: true,
          editable: true,
          autosuggest: {
            table: "oprema",
            key: "skupina",
            minLength: 0,
            tree_structure: true,
            tree_id: "skupina"
          }
        }
      ]
    },
    {
      key: "pn",
      text: "Part no.",
      new_ticket_info: true,
      input_type: "string",
      required: true
    },
    {
      key: "alter_pn",
      text: "Alt. part no.",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    },
    {
      key: "opis",
      text: "Opis",
      new_ticket_info: true,
      input_type: "text",
      editable: true
    },
    {
      key: "zaloga",
      text: "Zaloga",
      new_ticket_info: true,
      input_type: "integer",
      editable: true
    },
    {
      key: "enota",
      text: "Enota",
      new_ticket_info: true,
      input_type: "string",
      required: true
    },
    {
      key: "cena",
      text: "Cena (€)",
      new_ticket_info: true,
      input_type: "float",
      editable: true,
      required: true
    },
    {
      key: "rok_dobave",
      text: "Rok dobave (dan)",
      new_ticket_info: true,
      input_type: "integer",
      editable: true,
      required: true
    },
    {
      key: "artikel_opomba",
      text: "Komentar",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }
  ]
}
