globals.variable_settings = {
  urls: {
    api_url: "../ajax_simple_table/retrieve.php",
    create_url: "../ajax_simple_table/create.php",
    remove_url: "../ajax_simple_table/remove.php",
    get_selected_url: "../ajax_simple_table/getSelected.php",
    update_url: "../ajax_simple_table/update.php"
  },
  texts: {
    create: "Dodaj prevoznika",
    edit: "Uredi prevoznika"
  },
  key: "prevoznik",
  id_key: "id_prevoznik",
  text: "Osnovni podatki",
  data: [
    {
      key: "prevoznik",
      text: "Ime prevoznika",
      new_ticket_info: true,
      input_type: "string",
      required: true
    },
    {
      key: "telefon",
      text: "Telefon",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true
    },
    {
      key: "email",
      text: "Email naslov",
      new_ticket_info: true,
      input_type: "string",
      editable: true,
      required: true
    },
    {
      key: "prevoznik_opomba",
      text: "Komentar",
      new_ticket_info: true,
      input_type: "string",
      editable: true
    }
  ]
}
