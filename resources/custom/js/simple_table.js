let simple_table_urls = {
  api_url: "../ajax_simple_table/retrieve.php",
  create_url: "../ajax_simple_table/create.php",
  remove_url: "../ajax_simple_table/remove.php",
  get_selected_url: "../ajax_simple_table/getSelected.php",
  update_url: "../ajax_simple_table/update.php"
}

function makeSimpleTable(actionsList=[]) {

  let columns = [];
  for (var i=0; i<globals.variable_settings.data.length; i++) {
    let data = globals.variable_settings.data[i];
    columns.push({data: data.table_view_key || data.key, title: data.text, defaultContent: "None"});
  }

  let buttons = [
    {
      text: '<i class="fas fa-plus-circle"></i> '+globals.variable_settings.texts.create,
      action: function ( e, dt, node, config ) {
        $("#modal").modal("show");
        dodajVnos();
      },
      init: function(api, node, config) {
        $(node).removeClass('dt-button')
      },
      className: "btn btn-custom"
    }
  ];

  let table = document.createElement("table");
  $(table).addClass("table table-striped wrap dataTable dtr-inline collapsed");
  $(table).attr("id","table-"+globals.variable_settings.key);
  $(".table-wrapper").append(table);

  if (actionsList.length > 0) {
    columns[columns.length-1].fnCreatedCell = function (nTd, sData, oData, iRow, iCol) {
      $(nTd).html(sData);

      let actions = document.createElement("div");
      $(actions).addClass("dropdown");
      $(actions).attr("style","margin-left:10px; float: right; display: inline-block;");
      $(nTd).append(actions);

      let i = document.createElement("i");
      $(i).addClass("fas fa-cog dropdown-toggle").attr("data-toggle","dropdown").attr("aria-haspopup","true").attr("aria-expanded","false");
      $(actions).append(i);

      let actionsListDiv = document.createElement("div");
      $(actionsListDiv).addClass("dropdown-menu dropdown-menu-right");
      $(actions).append(actionsListDiv);

      if (actionsList.indexOf("edit") != -1) {
        let a = document.createElement("a");
        $(a).addClass("dropdown-item").attr("href","#");
        $(a).append("Uredi");
        $(a).click(function() {
          let params = {};
          let id_key = globals.variable_settings.id_key;
          params.id_key = id_key;
          params.id_value = oData[id_key];
          params.table = globals.variable_settings.key;
          urediVnos(params);
        });
        $(actionsListDiv).append(a);
      }
      if (actionsList.indexOf("remove") != -1) {
        let a = document.createElement("a");
        $(a).addClass("dropdown-item").attr("href","#");
        $(a).append("Izbriši");
        $(a).click((function(table) {
          return function() {
            let confirm_message = globals.variable_settings.texts.confirm_remove || "Ali želiš izbrisati vnos?";
            if (confirm(confirm_message) == true) {
              let params = {};
              let id_key = globals.variable_settings.id_key;
              params.id_key = id_key;
              params.id_value = oData[id_key];
              params.table = globals.variable_settings.key;
              Ajax(simple_table_urls.remove_url,params, function() {
                $(table).DataTable().ajax.reload(null,false);
              });
            }
          }
        })(table));
        $(actionsListDiv).append(a);
      }
    }
  }

  var tableContent = $(table).DataTable({
    serverSide: true,
    ajax: {
      url: simple_table_urls.api_url,
      type: 'POST',
      dataType: 'JSON',
      data: {
        table: globals.variable_settings.key,
        key_id: globals.variable_settings.id_key
      }
    },
    language: {
      emptyTable: "Ni prevoznikov."
    },
    dom: "<'row mb-2'<'col-md-12'B>><'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
    columns: columns,
    buttons: buttons
  })

  return tableContent;
}

function dodajVnos() {

  openModal("modal",{size: "lg", clear: true, title: '<i class="fas fa-plus-circle"></i> '+globals.variable_settings.texts.create});

  let wrapperDiv = document.createElement("div");
  $(wrapperDiv).css("width","100%");
  $("#modal .modal-body").append(wrapperDiv);

  let dataToFill = {rows: [{columns: [{data: []}]}]}

  for (var i=0; i<globals.variable_settings.data.length; i++) {
    let data = globals.variable_settings.data[i];
    if (data.concatenated) {
      for (var j=0; j<data.concat_parts.length; j++) {
        let data_inner = data.concat_parts[j];
        dataToFill.rows[0].columns[0].data.push(data_inner);
      }
    }
    else {
      dataToFill.rows[0].columns[0].data.push(data);
    }
  }

  let div = document.createElement("div");
  fillDetails(div,null,dataToFill,"new");

  $(wrapperDiv).append(div);

  let buttonSave = document.createElement("button");
  $(buttonSave).addClass("btn btn-custom");
  $(buttonSave).addClass("save-button")
  $(buttonSave).append("Shrani");
  $(buttonSave).click(function() {
    let div = $("#modal .modal-body");
    let prevoznik = {}

    readSection(div,{rows: [{columns: [{data: globals.variable_settings.data}]}]}, function(key,value) {
      prevoznik[key] = value
    })

    if ($(div).find(".missing-value").length == 0) {
      Ajax(simple_table_urls.create_url,{table: globals.variable_settings.key, data: prevoznik}, function(results) {
        $("#table-"+globals.variable_settings.key).DataTable().ajax.reload(null,true);
        if (typeof sendVnosToParent == "function") {
          sendVnosToParent(results)
        }
      });
      $("#modal").modal("hide");
    }
  })

  $("#modal .modal-footer").html("").append(buttonSave);
}

function urediVnos(params) {

  Ajax(simple_table_urls.get_selected_url, params, function(rowInfo) {

    openModal("modal",{clear: true, title: '<i class="fas fa-plus-circle"></i> '+globals.variable_settings.texts.edit});

    let wrapperDiv = document.createElement("div");
    $(wrapperDiv).css("width","100%");
    $("#modal .modal-body").append(wrapperDiv)

    let div = document.createElement("div");
    fillDetails(div,JSON.parse(rowInfo),{rows: [{columns: [{data: globals.variable_settings.data}]}]},"edit");

    $(wrapperDiv).append(div);

    let buttonSave = document.createElement("button");
    $(buttonSave).addClass("btn btn-custom");
    $(buttonSave).append("Shrani");
    $(buttonSave).click(function() {
      let div = $("#modal .modal-body");
      let prevoznik = {}

      readSection(div,{rows: [{columns: [{data: globals.variable_settings.data}]}]}, function(key,value) {
        prevoznik[key] = value
      })

      if ($(div).find(".missing-value").length == 0) {
        Ajax(simple_table_urls.update_url,{table: globals.variable_settings.key, data: prevoznik, id_key: params.id_key, id_value: params.id_value}, function(results) {
          $("#table-"+globals.variable_settings.key).DataTable().ajax.reload(null,true);
        });
        $("#modal").modal("hide");
      }
    })

    $("#modal .modal-footer").html("").append(buttonSave);
  })

}
