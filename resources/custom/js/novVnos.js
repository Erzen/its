function odpriDodajVnos(options) {

  $("#modal").modal("show");

  $("#modal .modal-dialog").css("max-width","100%").css("min-height","100%").css("margin","0");
  $("#modal .modal-content").css("min-height","100%");
  if (options) {
    dodajVnos(options);
  }
  else {
    dodajVnos();
  }
}

function sendVnosToParent(id_vnosa) {
  window.parent.postMessage(id_vnosa);
}
