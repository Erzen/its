function fillDetails(divToInsert,details={},settings,type) {
  let divWrapper = document.createElement("div");
  $(divWrapper).addClass("modal-content-inputs")
  $(divToInsert).append(divWrapper);

  let rows = settings.rows;

  for (var i=0; i<rows.length; i++) {
    let divRow = document.createElement("div");
    $(divRow).addClass("row details-row");
    $(divWrapper).append(divRow);

    let columns = rows[i].columns;

    for (var j=0; j<columns.length; j++) {
      let divColumn = document.createElement("div");
      $(divColumn).addClass("col")

      let data = columns[j].data;

      let detailsCounter = 0;

      for (var k=0; k<data.length; k++) {

        let detailWrapper

        if (data[k].input_type == "tree-structure" && details) {

          let treeData = data[k].data;
          let oprema_value = [];
          for (let l=0; l<treeData.length; l++) {
            if (details) {
              if (details[treeData[l].key] && details[treeData[l].key] != "") {
                oprema_value.push(details[treeData[l].key]);
              }
            }
          }

          detailWrapper = fillOneDetail(data[k],type,{nalog_id_key_value: details[globals.variable_settings.nalog_id_key], data_value: oprema_value.join(" "), details: details});
        }
        else if (data[k].autosuggest && details) { // autosuggest or dropwdown
          let data_value = {};
          data_value.value = details[data[k].key];
          if (data[k].autosuggest.by_id) {
            data_value.id = details[data[k].autosuggest_save.key];
          }
          detailWrapper = fillOneDetail(data[k],type,{nalog_id_key_value: details[globals.variable_settings.nalog_id_key], data_value: data_value});
        }
        else if (details) {
          detailWrapper = fillOneDetail(data[k],type,{nalog_id_key_value: details[globals.variable_settings.nalog_id_key], data_value: details[data[k].key]});
        }
        else {
          detailWrapper = fillOneDetail(data[k],type);
        }

        if (detailWrapper) {
          if (data[k].key == "oiw_partner_id" || data[k].key == "oiw_partner_ticket") {
            $(detailWrapper).addClass("oiw-suboptions detail-hidden");
            if (details && details.garancija == "OIW") {
              $(detailWrapper).removeClass("detail-hidden");
            }
          }
          if (data[k].key == "iw_status") {
            $(detailWrapper).addClass("iw-suboptions detail-hidden");
            if (details && details.garancija == "IW") {
              $(detailWrapper).removeClass("detail-hidden");
            }
          }

          $(divColumn).append(detailWrapper);
          detailsCounter++;
        }
      }

      if (detailsCounter > 0) {
        $(divRow).append(divColumn);
      }

    }
  }
}

function fillOneDetail(data,type,options) {

  let details_nalog_id_key = (options || {}).nalog_id_key_value;
  let data_cont = (options || {}).data_value;

  let detailsTrue = false;

  let valueRequired = (data.required && (type == "new" || type == "edit"))

  let detailWrapper = document.createElement("div");

  let spanTitle = document.createElement("span");
  $(spanTitle).addClass("bold-text");
  $(spanTitle).append((valueRequired ? "* " : "")+data.text+": ");
  $(detailWrapper).append(spanTitle);

  let spanContent = document.createElement("span");
  $(spanContent).addClass("details-content");
  $(detailWrapper).append(spanContent);

  if (type == "table") { // IN TABLE SHOW ALL DETAILS

    if ((data.input_type == "file" && data_cont)) {
      let downloadFile = formFiller(data,"download_file",{value: data_cont, type: type});
      $(downloadFile).css("display","inline-block");
      $(spanContent).append(downloadFile);
    }
    else if (data.input_type == "kosovnica") {
      if (data_cont && data_cont > 0) {
        $(spanContent).append(data_cont+" "+(data_cont == 1 ? "artikel" : (data_cont == 2 ? "artikla" : (data_cont < 5 ? "artikli" : "artiklov"))));
      }
      else {
        $(spanContent).append("0 artiklov");
      }
    }
    else {
      $(spanContent).append((data_cont != undefined) ? (data_cont.value || data_cont) : "NaN");
    }
    if (((data.editable != false) && !(dataExists(data_cont))) || data.editable == true) {
      let editButton = document.createElement("i");
      $(editButton).addClass("fas fa-pencil-alt icon-link edit-link");
      $(editButton).click((function(data,value,id) {
        return function(event) {
          event.stopPropagation();
          if (data && data.input_type == "kosovnica") {
            openModal("modal",{clear: true, size: "lg"});
          }
          else {
            openModal("modal",{clear: true});
          }

          if (data && data.input_type == "tree-structure") {
            $("#modal .modal-body").html("").append(formFiller(data,data.input_type,{details: options.details}));
          }
          else if (data && data.input_type == "kosovnica") {
            $("#modal .modal-body").html("").append(formFiller(data,data.input_type,{nalog_id: options.nalog_id_key_value}));
          }
          else {
            $("#modal .modal-body").html("").append(formFiller(data,data.input_type,{value: value}));
          }

          let buttonSave = document.createElement("button");
          $(buttonSave).addClass("btn btn-custom");
          $(buttonSave).append("Shrani");
          $(buttonSave).click(function() {
            let div = $("#modal .modal-body");
            let nalog = preberiNalog(div, function(nalog) {
              if ($(div).find(".missing-value").length == 0) {
                api_functions.urediNalog({id: id, nalog: nalog});
                $("#modal").modal("hide");
                $("#table-nalog").DataTable().ajax.reload(null,false);
              }
            });
          })

          $("#modal .modal-footer").html("").append(buttonSave);
        }
      })(data,data_cont,details_nalog_id_key))
      $(spanContent).append(editButton);
    }
    detailsTrue = true;
  }
  else if (type == "new" && data.new_ticket_info) { // IN NEW TICKET SHOW ONLY DETAILS THAT HAVE TO BE FILLED
    $(spanContent).append(formFiller(data,data.input_type));
    detailsTrue = true;
  }
  else if (type == "edit") {

    let input_type = (data.input_type == "file" && data_cont) ? "download_file" : data.input_type;
    let input;

    if (data && data.input_type == "tree-structure") {
      input = formFiller(data,data.input_type,{details: options.details});
    }
    else if (data && data.input_type == "kosovnica") {
      input = document.createElement("span");
      if (data_cont && data_cont > 0) {
        $(input).append(data_cont+" "+(data_cont == 1 ? "artikel" : (data_cont == 2 ? "artikla" : (data_cont < 5 ? "artikli" : "artiklov"))));
      }
      else {
        $(input).append("0 artiklov");
      }

      let btnUredi = document.createElement("button");
      $(btnUredi).attr("type","button");
      $(btnUredi).addClass("btn btn-custom-outline");
      $(btnUredi).css("margin-left","10px");
      $(btnUredi).append("<span><i class='fas fa-pencil-alt'></i> Uredi kosovnico</span>");
      $(btnUredi).click(function() {
        openModal("modal-second",{clear: true, size: "lg", title: data.text});
        $("#modal-second .modal-body").html("").append(formFiller(data,data.input_type,{nalog_id: options.nalog_id_key_value}));
      })
      $(input).append(btnUredi);

      //input = formFiller(data,data.input_type, {nalog_id: options.nalog_id_key_value});
    }
    else {
      input = formFiller(data,data.input_type, {value: options.data_value});
    }

    if (data.editable == false || (data.editable != true && dataExists(data_cont))) {
      $(input).attr("disabled",true);
    }
    $(spanContent).append(input)
    detailsTrue = true;

  }

  if (detailsTrue) {
    return detailWrapper;
  }
  else {
    return false;
  }
}

function formFiller(data,type,options) {

  let key = data.key;

  options = options || {};

  let actualInputDiv;
  let inputGroup;

  if (type == "string") {
    inputGroup = document.createElement("input");
    $(inputGroup).attr("type","text").attr("placeholder","");
    $(inputGroup).addClass("form-control");
    if (options.value != undefined) {
      $(inputGroup).val(options.value);
    }
    actualInputDiv = inputGroup;
  }
  else if (type == "integer") {
    inputGroup = document.createElement("input");
    $(inputGroup).attr("type","number").attr("min",0);
    $(inputGroup).addClass("form-control");
    $(inputGroup).val((options.value != undefined ? options.value : 0));
    actualInputDiv = inputGroup;
  }
  else if (type == "float") {
    inputGroup = document.createElement("input");
    $(inputGroup).attr("type","number").attr("min",0).attr("step",0.01);
    $(inputGroup).addClass("form-control");
    $(inputGroup).val((options.value != undefined ? options.value : 0));
    actualInputDiv = inputGroup;
  }
  else if (type == "text") {
    inputGroup = document.createElement("textarea");
    $(inputGroup).attr("rows","5").attr("style","width: 100%");
    if (options.value != undefined) {
      $(inputGroup).val(options.value);
    }
    actualInputDiv = inputGroup;
  }
  else if (type == "file") {
    inputGroup = document.createElement("div");
    $(inputGroup).addClass("custom-file");
    let input = document.createElement("input");
    $(input).attr("type","file");
    $(input).addClass("custom-file-input");
    $(input).on('change',function(ev) {
      //write file name to window
      var fileName = $(ev.target).val().replace('C:\\fakepath\\', " ");
      $(ev.target).next('.custom-file-label').html(fileName);

      let parentSpan = $(this).closest("span.details-content");
      options.value = fileName;
      data.editable = true;
      $(parentSpan).html("").append(formFiller(data,"download_file",options));
    })
    $(inputGroup).append(input);
    $(inputGroup).append('<label class="custom-file-label" for="custom-input-id-'+key+'">Choose file</label>')

    actualInputDiv = input;
  }
  else if (type == "download_file") {
    inputGroup = document.createElement("div");
    if (options.value != undefined) {

      let aFile = document.createElement("a");
      $(aFile).attr("href","#");
      $(aFile).addClass("icon-link a-link")
      $(aFile).append('<i class="fas fa-download"></i> '+options.value);
      $(aFile).click(function() {
        api_functions.downloadFile(options.value);
      });
      $(inputGroup).append(aFile);

      if (data.editable == true && options.type != "table") {
        let aRemove = document.createElement("a");
        $(aRemove).attr("href","#");
        $(aRemove).addClass("a-link a-icon-remove")
        $(aRemove).append('<i class="fas fa-times"></i>');
        $(aRemove).click(function() {
          let parentSpan = $(this).closest("span.details-content");
          options.value = undefined;
          $(parentSpan).html("").append(formFiller(data,"file",options));
        })
        $(inputGroup).append(aRemove);
      }
    }
  }
  else if (type == "dropdown") {
    inputGroup = document.createElement("select");
    $(inputGroup).addClass("custom-select my-1 mr-sm-2");
    $(inputGroup).attr("select-group","custom-input-group-"+key);

    let url = "../ajax_simple_table/autosuggest.php?term=''";

    for (let autosuggest_key in data.autosuggest) {
      url += "&autosuggest["+autosuggest_key+"]="+data.autosuggest[autosuggest_key];
    }

    Ajax(url, {}, function(results) {
      results = JSON.parse(results);
      let primaryOption;
      if (data.autosuggest.default == undefined || data.autosuggest.default == false) {
        primaryOption = document.createElement("option");
        $(primaryOption).attr("selected",true);
        $(primaryOption).attr("value","");
        $(primaryOption).append("Choose...");
      }
      $(inputGroup).append(primaryOption);

      for (let i=0; i<results.length; i++) {
        let option = document.createElement("option");
        if (options.value != undefined && ((data.autosuggest.by_id == true && options.value.id == results[i].id) || (data.autosuggest.by_id != true && options.value.value == results[i].value))) {
          $(option).attr("selected",true);
          $(primaryOption).attr("selected",false);
        }
        $(option).attr("value",(data.autosuggest.by_id ? results[i].id : results[i].value));
        $(option).append(results[i].value);
        $(inputGroup).append(option);
      }
    })

    if (options.value != undefined) {
      let value = data.autosuggest.by_id ? options.value.id : options.value.value
      $(inputGroup).val(value);
    }

    if (data.multiple_occurence) {
      $(inputGroup).on('change', function() {
        let others = $("select[select-group='custom-input-group-"+key+"']").not(this);

        for (let i=0; i<others.length; i++) {
          if (others[i].value != this.value) {
            $(others[i]).val(this.value);
          }
        }

        if (data.key == "garancija") {
          if (this.value == "OIW") {
            $(".oiw-suboptions").removeClass("detail-hidden");
            $(".iw-suboptions").addClass("detail-hidden");
          }
          else if (this.value == "IW") {
            $(".iw-suboptions").removeClass("detail-hidden");
            $(".oiw-suboptions").addClass("detail-hidden");
          }
          else {
            $(".oiw-suboptions, .iw-suboptions").addClass("detail-hidden");
          }
        }
      });
    }

    actualInputDiv = inputGroup;

  }
  else if (type == "date") {
    inputGroup = document.createElement("input");
    $(inputGroup).attr("id","datepicker-"+key).attr("width","100%");
    $(inputGroup).ready(function() {
      $(inputGroup).datepicker({
        uiLibrary: "bootstrap4",
        format: 'dd/mm/yyyy'
      });
      if (options.value != undefined) {
        let date = options.value.split(" ")[0].split("-");
        let yy = date[0];
        let mm = date[1];
        let dd = date[2];
        date = dd+"/"+mm+"/"+yy;
        $(inputGroup).datepicker().value(date);
      }
      else {
        let today = new Date();
        let yyyy = today.getFullYear();
        let mm = today.getMonth()+1;
        let dd = today.getDate();

        //$(inputGroup).datepicker().value(dd+"/"+mm+"/"+yyyy);
      }

      if (data.editable == false || (data.editable != true && dataExists(options.value))) {
        $(inputGroup).next("span").find("button").attr("disabled",true);
      }
    })
    actualInputDiv = inputGroup;
  }
  else if (type == "autosuggest") {

    let editable;
    if (options.tree_data) {
      editable = true;
    }
    else {
      editable = options.editable || (data.editable == true || (data.editable != false && (options.value == undefined || (options.value.value == undefined || options.value.value == null))));
    }

    let minLength = (data.autosuggest.minLength != undefined) ? data.autosuggest.minLength : 2;

    inputGroup = document.createElement("div");
    $(inputGroup).addClass("form-group mb-3 query-input ui-widget");
    let input = document.createElement("input");
    $(input).attr("type","text");
    $(input).addClass("form-control");
    $(input).click(function(event) {
      event.preventDefault();
    });
    $(inputGroup).append(input);

    $(input).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "../ajax_simple_table/autosuggest.php",
          data: {
            term: request.term,
            autosuggest: data.autosuggest,
            extra_conditions: (options.tree_data ? extraAutosuggestParams(options.tree_data,{input: input}) : [])
          },
          success: function( data ) {
            response( JSON.parse(data) );
          }
        } );
      },
      minLength: minLength,
      select: function( event, ui ) {
        if (options.tree_data) {
          let conditions = extraAutosuggestParams(options.tree_data, {item: {key: data.autosuggest.tree_id || data.autosuggest.key, value: ui.item.id || ui.item.value}, empty_vals: true});
          getTreeViewTextBarInfo({table: data.autosuggest.tree_table || data.autosuggest.table, conditions: conditions}, function(results) {
            let infoBar = $(input).closest("div.tree-view").find("div.info-text");
            fillTreeInfoBar(JSON.parse(results),infoBar,data);
          })
        }
        $(".ui-menu-item").hide();
      },
      change: function (event, ui) {
        if (!ui.item && data.autosuggest.must_exist_in_db != false) {
          $(this).val("");
          alert("Vpisane opcije še ni v bazi.")
        }
        if (options.filterDiv != true && data.autosuggest_onchange) {
          data.autosuggest_onchange(ui)
        }
      }
    });

    if (options.value != undefined) {
      $(input).data("uiAutocomplete").selectedItem = options.value;
      $(input).val(options.value.value);
    }

    if (!(editable)) {
      $(input).attr("disabled",true);
    }

    if (options.filterDiv != true && editable && data.add_new) {
      let aAddNew = addIframe(data,inputGroup);
      $(inputGroup).append(aAddNew);
    }

    actualInputDiv = input;
  }
  else if (type == "tree-structure") {

    let editable = data.editable == true || (data.editable != false && (options.details == undefined || (options.details && !(dataExists(options.details[data.key])))));

    inputGroup = document.createElement("div");
    $(inputGroup).addClass("tree-view");

    let conditions = [];

    for (let i=0; i<data.data.length; i++) {
      let treeData = data.data[i];

      let data_value = {};
      if (options.details) {
        data_value.value = options.details[treeData.key];
        if (treeData.autosuggest.by_id) {
          data_value.id = options.details[treeData.by_id_key];
        }

        let tree_id = treeData.autosuggest.tree_id || treeData.autosuggest.key;
        let details_id = treeData.custom_var ? "cvar_"+tree_id : tree_id;
        let tree_value = options.details[details_id] ? (options.details[details_id] == "" ? "null" : options.details[details_id]) : "null";
        conditions.push({key: tree_id, value: tree_value});
      }

      let valueRequired = treeData.required;

      let detailWrapper = document.createElement("div");
      $(inputGroup).append(detailWrapper);

      let spanTitle = document.createElement("span");
      $(spanTitle).addClass("bold-text");
      $(spanTitle).append((valueRequired ? "* " : "")+treeData.text+": ");
      $(detailWrapper).append(spanTitle);

      let spanContent = document.createElement("span");
      $(spanContent).addClass("details-content");
      $(detailWrapper).append(spanContent);
      let form = formFiller(treeData,treeData.input_type,{tree_data: data.data, value: data_value});
      $(spanContent).append(form);

      if (!(editable)) {
        $(form).find("input").attr("disabled",true);
      }
    }
    let infoText = document.createElement("div");
    $(infoText).addClass("info-text");
    $(inputGroup).append(infoText);

    let infoTextInput = document.createElement("input");
    $(infoTextInput).addClass("info-text-input");
    $(infoTextInput).css("display","none");
    $(infoTextInput).attr("id","custom-input-id-"+data.key);
    $(infoText).append(infoTextInput);

    let infoTextContent = document.createElement("div");
    $(infoTextContent).addClass("info-text-content");
    $(infoText).append(infoTextContent);

    if (options.details && dataExists(options.details[data.key])) {
      getTreeViewTextBarInfo({table: data.data[0].autosuggest.tree_table || data.data[0].autosuggest.table, conditions: conditions}, function(results) {
        fillTreeInfoBar(JSON.parse(results),infoText,data.data);
      })
    }
    else {
      $(infoTextContent).append("Iskalni parametri niso vnešeni.");
    }

    if (!(editable)) {
      $(infoText).css("display","none");
    }

    if (editable && data.add_new) {
      let aAddNew = addIframe(data,inputGroup);
      $(inputGroup).append(aAddNew);
    }
  }
  else if (type == "kosovnica") {
    inputGroup = makeKosovnicaTable(data,options);
  }
  else {
    inputGroup = document.createElement("input");
    $(inputGroup).attr("type","text").attr("placeholder","");
    $(inputGroup).addClass("form-control");
    actualInputDiv = inputGroup;
  }

  if (actualInputDiv) {
    let id = (options.filterDiv ? "table-filters-" : "custom-input-id-")+key;
    $(actualInputDiv).attr("id",id);

    if (data.required) {
      $(actualInputDiv).addClass("needs-validation");
    }

  }
  return inputGroup;
}

function extraAutosuggestParams(tree_data,options) {

  let curr_id = (options && options.input) ? options.input.id : "";
  let conditions = [];

  for (let i=0; i<tree_data.length; i++) {
    let new_id = "custom-input-id-"+tree_data[i].key;
    if (new_id != curr_id) {
      let key = tree_data[i].autosuggest.tree_id || tree_data[i].key; // trenutno samo za autosuggest!
      if (options.item && options.item.key == key) {
        conditions.push(options.item);
      }
      else {
        let value = readForm($("#"+new_id),tree_data[i]);
        if (value != undefined) {
          conditions.push({key: key, value: value});
        }
        else if (options.empty_vals) {
          conditions.push({key: key, value: "null"});
        }
      }
    }
  }

  return conditions;
}

function fillTreeInfoBar(results,infoBar,options) {

  let infoBarText = $(infoBar).find(".info-text-content");
  let infoBarInput = $(infoBar).find(".info-text-input");

  let allMatches = 0;
  let exactMatches = 0;
  let exactMatch;

  for (let i=0; i<results.length; i++) {
    if (results[i].exact_match == 1) {
      exactMatches++;
      exactMatch = results[i].id_oprema;
    }
    allMatches++;
  }

  $(infoBarText).html("");
  if (exactMatches > 1) {
    $(infoBarText).append("<div class='alert alert-danger'>Pozor: Najdenih je bilo več("+exactMatches+") oprem z identičnimi paramteri! Preveri tabelo opreme!</div>");
  }
  if (allMatches > 1) {
    $(infoBarText).append("<div class='alert alert-info'>Prametri ustrezajo "+allMatches+" "+(allMatches == 1 ? "opremi" : (allMatches == 2 ? "opremama" : "opremam"))+". Določi ostale parametre, da zožaš iskanje.</div>")
  }
  if (exactMatches == 1 || allMatches == 1) {
    exactMatch = exactMatch || results[0].id_oprema;
    $(infoBarInput).val(exactMatch);
    $(infoBarText).append("<div class='alert alert-success'>Oprema najdena!</div>");

    // SPODNJE PRIDE V POŠTEV LE, ČE BOMO IZPISALI CEL REZULTAT KO BO SAMO 1 OPICJA AMPAK NE BO ISTA TRENUTNEMU VNOSU!
    /*let table = options[0].autosuggest.tree_table || options[0].autosuggest.table;

    Ajax("../ajax_simple_table/getSelected.php",{table: table, id_value: exactMatch}, function(match) {
    console.log(JSON.parse(match))
  })*/
}
}

function getTreeViewTextBarInfo(params,cb) {
  Ajax("../ajax_simple_table/check_existence.php",params, function(results) {
    cb(results);
  })
}

function readSection(div,section,cb) {
  for (var i=0; i<section.rows.length; i++) {
    for (var j=0; j<section.rows[i].columns.length; j++) {
      for (var k=0; k<section.rows[i].columns[j].data.length; k++) {
        let key = section.rows[i].columns[j].data[k].key
        let input = $(div).find("#custom-input-id-"+key);
        if (input.length == 1 && ($(input).closest(".detail-hidden").length == 0)) { // if input exists and is not intentionally hidden aka no value possible
          let value = readForm(input[0],section.rows[i].columns[j].data[k]);
          if (section.rows[i].columns[j].data[k].input_type == "autosuggest") {
            if (section.rows[i].columns[j].data[k].autosuggest_save && section.rows[i].columns[j].data[k].autosuggest_save.key) {
              key = section.rows[i].columns[j].data[k].autosuggest_save ? section.rows[i].columns[j].data[k].autosuggest_save.key : key;
              key = section.rows[i].columns[j].data[k].autosuggest_save.key;
              if (value != undefined) {
                cb(key,value);
              }
            }
            else if (section.rows[i].columns[j].data[k].autosuggest_save && section.rows[i].columns[j].data[k].autosuggest_save.key_function) {
              section.rows[i].columns[j].data[k].autosuggest_save.key_function(function(pairs) {
                for (let p=0; p<pairs.length; p++) {
                  if (pairs[p].value != undefined) {
                    cb(pairs[p].key,pairs[p].value);
                  }
                }
              });
            }
            else {
              cb(key,value);
            }
          }
          else if (section.rows[i].columns[j].data[k].input_type == "dropdown") {
            key = section.rows[i].columns[j].data[k].autosuggest_save ? section.rows[i].columns[j].data[k].autosuggest_save.key : key;
            if (value != undefined) {
              cb(key,value);
            }
          }
          else {
            cb(key,value);
          }
        }
        else if (input.length == 0) {
          console.log("NO SUCH INPUT! "+"#custom-input-id-"+key)
          cb(key)
        }
        else if (input.length > 1) {
          console.log("MORE THAN ONE INPUT!")
          cb(key)
        }
      }
    }
  }
}

function readForm(input,options) {

  $(input).removeClass("missing-value");

  let value;

  if (options.input_type == "file") {
    if ($(input)[0].files[0]) {
      value = $(input)[0].files[0].name;
    }
  }
  else if (options.input_type == "autosuggest") {
    value = $(input).data("uiAutocomplete").selectedItem;
    if (value == null) {
      if (options.autosuggest.must_exist_in_db == false && $(input).val() != "") {
        value = $(input).val();
      }
      else {
        value = undefined;
      }
    }
    else {
      if (options.autosuggest.by_id) {
        value = $(input).data("uiAutocomplete").selectedItem.id;
      }
      else {
        value = $(input).data("uiAutocomplete").selectedItem.value;
      }
    }

  }
  else if (options.input_type == "date") {
    if ($(input).val() != "") {
      value = $(input).val().split("/");
      let yy = value[2];
      let mm = value[1];
      let dd = value[0];
      let today = new Date();
      value = yy+"-"+mm+"-"+dd+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();
    }
  }
  else if (options.input_type == "tree-structure") {
    $(input).closest(".tree-view").removeClass("missing-value");
    let inputs = $(input).closest(".tree-view").find("input").not(input);
    let inputs_values_join = inputs.map(function(idx, elem) {
      return $(elem).val();
    }).get().join("");
    if (inputs_values_join != "") {
      if ($(input).val() == "") {
        $(input).closest(".tree-view").addClass("missing-value");
        alert("Polje "+options.text+" ni izpolnjeno do konca!");
      }
      else {
        value = $(input).val();
      }
    }
    else if (options.required) {
      $(input).closest(".tree-view").addClass("missing-value");
    }
  }
  else {
    if ($(input).val() != "") {
      value = $(input).val();
    }
  }

  if (options.input_type == "file") {
    input = $("label[for='" + $(input).attr('id') + "']");
  }

  if (value == undefined && $(input).hasClass("needs-validation")) {
    $(input).addClass("missing-value");
  }
  else if ($(input)[0].validity.valid == false) {
    $(input).addClass("missing-value");
  }
  else if (value != undefined) {
    if (options.input_type == "integer" && options.minVal) {
      if (parseInt(value) < options.minVal) {
        $(input).addClass("missing-value");
      }
    }
  }

  return value;

}

function makeKosovnicaTable(data,options) {

  let tableWrapper = document.createElement("div");

  let stroski = document.createElement("div");
  $(stroski).addClass("container kosovnica-div");
  $(stroski).append('<div class="row"><div class="col">Dosedanji stroški:</div><div class="col"><span class="stroski-dosedanji">0.00</span>€</div></div><div class="row"><div class="col">Predvideni nadaljni stroški:</div><div class="col"><span class="stroski-predvideni">0.00</span>€</div></div><div class="row"><div class="col">Skupaj predvideni stroški:</div><div class="col"><span class="stroski-skupaj">0.00</span>€</div></div>')

  let table = document.createElement("table");
  $(table).addClass("table table-striped wrap dataTable dtr-inline collapsed");
  $(table).attr("id","table-kosovnica");
  $(tableWrapper).append(stroski);
  $(tableWrapper).append(table);

  let columns = [];
  let predvideni_stroski = 0;
  let trenutni_stroski = 0;
  for (let i=0; i<data.table.data.length; i++) {
    let colOptions = data.table.data[i];
    let column = {title: colOptions.text, data: colOptions.key};
    if (colOptions.key == "porabljeno_kosovnica") {
      column.fnCreatedCell = function (nTd, sData, oData, iRow, iCol) {
        let edit = document.createElement("i");
        $(edit).addClass("fas fa-pencil-alt icon-link edit-link");
        $(edit).click(function() {
          let inputGroup = document.createElement("input");
          $(inputGroup).attr("type","number").attr("min",0).attr("max",parseInt(oData.kolicina_kosovnica)).css("width","50%").css("display","inline-block");
          $(inputGroup).addClass("form-control");
          $(inputGroup).val(sData);
          $(nTd).html("").append(inputGroup);

          let save = document.createElement("button");
          $(save).addClass("btn btn-sm btn-custom").css("display","inline-block");
          $(save).append("Shrani");
          $(save).click(function() {
            Ajax(globals.variable_settings.update_url,{table: data.table.table_id, id_key: data.table.table_id_key, id_value: oData.id_kosovnica, data: {porabljeno_kosovnica: $(inputGroup).val()}}, function(results) {
              $("#table-kosovnica").DataTable().ajax.reload(null,true);
            });
          });
          $(nTd).append(" ").append(save);
        })
        $(nTd).html(sData+" ");
        $(nTd).append(edit);
      };
    }
    columns.push(column)
  }

  var tableContent = $(table).DataTable({
    serverSide: true,
    paging: false,
    ajax: {
      url: globals.variable_settings.api_url,
      type: 'POST',
      dataType: 'JSON',
      data: {
        table: data.table.table_id,
        key_id: data.table.table_id_key,
        nalog_id: options.nalog_id
      }
    },
    language: {
      emptyTable: "Kosovnica je prazna."
    },
    columns: columns
  })
  .on( 'draw', function (e, settings, drawData) {

    let tr = document.createElement("tr");
    $(tr).css("background-color","var(--hilight-color)");
    $(tr).attr("role","row");
    $(tr).addClass("even input-row");

    for (let i=0; i<data.table.data.length; i++) {
      let colOptions = data.table.data[i];
      let td = document.createElement("td");
      $(td).css("vertical-align","top").css("color","white");
      let input = formFiller(colOptions,colOptions.input_type);
      $(input).attr("style","margin: 0px !important");

      $(tr).append(td);

      if (i < data.table.data.length-1) {
        $(td).append(input);
      }

      else if (i == data.table.data.length-1) {
        let addBtn = document.createElement("a");
        $(addBtn).attr("href","#").attr("style","padding-top: 5px; font-weight: bold; float: right");
        $(addBtn).addClass("a-link");
        $(addBtn).append("<i class='fas fa-plus-circle'></i> Dodaj artikel");
        $(addBtn).click(function() {
          let artikel = {nalog_id: options.nalog_id};
          for (let i=0; i<data.table.data.length; i++) {
            let colOptions = data.table.data[i];
            let input = $("#custom-input-id-"+colOptions.key);

            if (input.length == 1 && ($(input).closest(".detail-hidden").length == 0)) { // if input exists and is not intentionally hidden aka no value possible
              let key = colOptions.key;
              let value = readForm(input,colOptions);
              if (colOptions.input_type == "autosuggest") {
                if (colOptions.autosuggest_save && colOptions.autosuggest_save.key) {
                  key = colOptions.autosuggest_save.key;
                  if (value != undefined) {
                    artikel[key] = value;
                  }
                }
                else {
                  artikel[key] = value;
                }
              }
              else {
                artikel[key] = value;
              }
            }
          }

          if ($(tr).find(".missing-value").length == 0) {
            // manjka vmes še workflow preverjanje!
            if (artikel) {
              Ajax(globals.variable_settings.create_url,{table: data.table.table_id, data: artikel}, function(results) {
                $("#table-kosovnica").DataTable().ajax.reload(null,true);
              });
            }
          }
        })
        $(td).append(addBtn);
      }
    }

    // izracunaj stroske
    let returned_data = settings.json.data;
    let stroski_dejanski = 0;
    let stroski_nadaljni = 0;
    for (let i=0; i<returned_data.length; i++) {
      let artikel_data = returned_data[i];
      let cena = parseFloat(artikel_data.cvar_cena || 0);
      let kolicina_predvidena = parseFloat(artikel_data.kolicina_kosovnica || 0);
      let kolicina_dejanska = parseFloat(artikel_data.porabljeno_kosovnica || 0);
      stroski_dejanski += kolicina_dejanska*cena;
      stroski_nadaljni += (kolicina_predvidena-kolicina_dejanska)*cena;
    }

    let stroski_skupno = stroski_dejanski+stroski_nadaljni;
    $(".stroski-dosedanji").html(stroski_dejanski)
    $(".stroski-predvideni").html(stroski_nadaljni)
    $(".stroski-skupaj").html(stroski_skupno);

    $(table).find("tbody").prepend(tr)
  } );

  return tableWrapper;

}

function addIframe(data,input) {

  modal_id = data.add_new_modal_id ? data.add_new_modal_id : "modal";

  let aAddNew = document.createElement("a");
  $(aAddNew).addClass("a-link icon-link");
  $(aAddNew).attr("href","#");
  $(aAddNew).click(function(modal_id) {

    return function() {
      let iframe = document.createElement("iframe");
      $(iframe).attr("style","width: 100%;").attr("src",data.add_new_url).attr("frameBorder","0");
      $(iframe).on("load", function() {
        iframeLoaded(this,modal_id,data,input)
      })

      //$("#modal .modal-header").css("display","none")
      $("#"+modal_id+" .modal-footer").css("display","none")
      $("#"+modal_id+" .modal-body").css("display","none")
      $("#"+modal_id+" .modal-content").append(iframe)
    }
  }(modal_id))
  $(aAddNew).append("<i class='fas fa-plus-circle'></i> Dodaj novo");

  return aAddNew;
}
