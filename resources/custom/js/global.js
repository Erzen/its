var globals = {
  constants: {
  },
  variables: {
    timeFlags: {
    }
  }
}

$(document).click(function(event) {

	var autoclose = $(".autoclose");
	if (!autoclose.is(event.target) && autoclose.has(event.target).length === 0) {
		autoclose.html("");
	}
});

$(document).on('hide.bs.modal','#modal,#modal-second', function () {
  $(this).find(".modal-footer").css("display","flex");
  $(this).find(".modal-body").css("display","block");
})

// basic ajax wrapper
function Ajax(url, input, callback) {
	var ajaxFun = $.ajax({
		url: url,
		data: input,
		type: ((input && !(jQuery.isEmptyObject(input))) ? "POST" : "GET"),
		//timeout: 15*60*1000,
		success: function (data) {
			callback(data)
		},
		error: function (xhr, status, error) {
			if (error != "abort") {
        try {
          let message = JSON.parse(xhr.responseText).message;
          alert(message);
        }
        catch(e) {
          console.log(error);
        }

				callback();
			}
		}
	});

	return ajaxFun;
}

/*window.addEventListener('load', function () { // autosuggest on global search
  console.log($("input#global-search"))
      $("input#global-search").autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: "../ajax_simple_table/autosuggest.php",
            data: {
              term: request.term,
              autosuggest: {global_search: true}
            },
            success: function( data ) {
              console.log(JSON.parse(data))
              response( JSON.parse(data) );
            }
          } );
        },
        minLength: 1,
        select: function( event, ui ) {
          $(".ui-menu-item").hide();
        }
      } );
}, false);*/




function openModal(id,options) {
  $("#"+id+" .modal-dialog").removeClass("modal-sm modal-lg modal-xl")
  if (options.size == "sm") {
    $("#"+id+" .modal-dialog").addClass("modal-sm");
  }
  else if (options.size == "lg") {
    $("#"+id+" .modal-dialog").addClass("modal-lg");
  }
  else if (options.size == "xl") {
    $("#"+id+" .modal-dialog").addClass("modal-lg modal-xl");
  }

  if (options.clear) {
    $("iframe").remove();
    $("#"+id+" .modal-body").html("");
  }

  if (options.title) {
    $("#"+id+" .modal-title").html(options.title);
  }

  $("#"+id).modal("show");
}

function getDateAsJson(date,delay,type="day") {

  if (delay) {
    if (type == "day") {
      date.setDate(date.getDate() + delay);
    }
    else if (type == "hour") {
      date.setDate(date.getHours() + delay);
    }
  }

  let yy = date.getFullYear();
  let mm = date.getMonth()+1;
  let dd = date.getDate();
  let hh = date.getHours();
  let min = date.getMinutes();
  let ss = date.getSeconds();

  return {yy: yy, mm: mm, dd: dd, hh: hh, min: min, ss: ss};
}

function dataExists(data) {
  return (data != undefined && data != null & data != "");
}

function iframeLoaded(obj,modal_id,options,inputGroup) {

  window.addEventListener('message', handleMessage, false);

  function handleMessage(event) {

    $(obj).remove();
    $("#"+modal_id+" .modal-footer").css("display","flex")
    $("#"+modal_id+" .modal-body").css("display","block")

    let data = JSON.parse(event.data);

    if (data.success) {
      fillWithNewValue(options,data.inserted.id,inputGroup);
    }
    else {
      alert(data.message+"\n"+data.sql);
    }
  }

  let iframe = obj.contentWindow.document.documentElement
  //obj.style.height = Math.max($(modal).find(".modal-dialog").height(),($(iframe).find(".modal-dialog")[0].scrollHeight + 4)) + 'px';
  obj.style.height = ($(iframe).find(".modal-dialog")[0].scrollHeight + 4) + 'px';

  let iframemodal = $(iframe).find(".modal")

  let closeButton = $(iframe).find("button.close")

  $(closeButton).click(function() {
    $(obj).remove();
    $("#"+modal_id+" .modal-footer").css("display","flex")
    $("#"+modal_id+" .modal-body").css("display","block")
  })
}

function fillWithNewValue(options,id_value,inputGroup) {

  let table_name = options.input_type == "autosuggest" ? options.autosuggest.table : (options.input_type == "tree-structure" ? options.table_view_key : false);
  let get_selected_url = typeof simple_table_urls !== 'undefined' ? simple_table_urls.get_selected_url : globals.variable_settings.get_selected_url;

  if (table_name != false) {
    Ajax(get_selected_url,{table: table_name, id_value: id_value}, function(results) {
      results = JSON.parse(results);

      let mainInput = $(inputGroup).find("#custom-input-id-"+options.key);

      if (options.input_type == "autosuggest") {
        let mainValue = results[options.autosuggest.key]
        $(mainInput).data("uiAutocomplete").selectedItem = {id: id_value, value: mainValue};
        $(mainInput).val(mainValue);
      }

      if (options.input_type == "tree-structure") {
        $(mainInput).val(id_value);
        for (let i=0; i<options.data.length; i++) {
          let data = options.data[i];
          let input = $(inputGroup).find("#custom-input-id-"+data.key);
          let value;

          if (data.autosuggest.by_id) {
            value = results[data.key];
            let id = results[data.autosuggest.tree_id];
            $(input).data("uiAutocomplete").selectedItem = {id: id, value: value};
          }
          else {
            value = results[data.autosuggest.tree_id];
            $(input).data("uiAutocomplete").selectedItem = {value: value};
          }
          $(input).val(value);
        }
      }
    });
  }
}
